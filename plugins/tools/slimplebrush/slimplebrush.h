#ifndef SLIMPLEBRUSH_H
#define SLIMPLEBRUSH_H

#include "slimplebrush_global.h"

//SLIMPLEBRUSHSHARED_EXPORT
class  SimpleBrush : public PaintTool {
    //Q_OBJECT
public:
    explicit SimpleBrush(PaintTool *parent=0);
    ~SimpleBrush();

    bool load(QString name) override;
    bool save(QString name) override;
    void setupUi();

    void action(QList<Dot> stroke, QImage* image) override;
    void action(QList<Dot> stroke) override;

signals:

public slots:

private:

    QFormLayout *layout;
    QVBoxLayout *toolSettings;

};

#endif // SLIMPLEBRUSH_H
