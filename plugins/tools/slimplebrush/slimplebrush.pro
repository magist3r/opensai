#-------------------------------------------------
#
# Project created by QtCreator 2013-05-11T22:54:58
#
#-------------------------------------------------

QT       += gui

QMAKE_CXXFLAGS += -std=c++11

TARGET = slimplebrush
TEMPLATE = lib

DEFINES += SLIMPLEBRUSH_LIBRARY

SOURCES += slimplebrush.cpp

HEADERS += slimplebrush.h\
        slimplebrush_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
