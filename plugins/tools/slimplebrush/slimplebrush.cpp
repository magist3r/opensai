#include "slimplebrush.h"




SimpleBrush::SimpleBrush(PaintTool *parent)
    :PaintTool(parent)
{
}

SimpleBrush::~SimpleBrush() {}

bool SimpleBrush::load(QString)
{
    return true;
}
bool SimpleBrush::save(QString) {
    return true;
}
void SimpleBrush::setupUi()
{}

void SimpleBrush::action(QList<Dot> stroke, QImage* image)
{
    QPainter p(image);
    QPen pen;
    pen.setColor(color);
    pen.setWidth(1);
    p.setPen(pen);

    for(Dot& itr : stroke) {
        p.drawPoint(itr.pos);
        p.setOpacity(itr.pressure);
        qDebug(QString::number(itr.pos.rx()).toAscii());
    }
}

void SimpleBrush::action(QList<Dot> stroke)
{
    Q_UNUSED(stroke);
}
