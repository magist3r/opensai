#-------------------------------------------------
#
# Project created by QtCreator 2013-03-24T23:25:38
#
#-------------------------------------------------

QT       += core
QT       += gui

TARGET = layers
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11


SOURCES += main.cpp \
    layerset.cpp \
    rendertree.cpp \
    layertest.cpp \
    blendmode.cpp \
    layerimage.cpp \
    abstractlayer.cpp

HEADERS += \
    layer.h \
    layerset.h \
    rendertree.h \
    layertest.h \
    blendmode.h \
    layerimage.h \
    abstractlayer.h
