#ifndef LAYERSET_H
#define LAYERSET_H

//Qt
#include <QList>

//Own
#include "rendertree.h"
#include "abstractlayer.h"

typedef QList<AbstractLayer*> LayerList;

class LayerSet : public AbstractLayer, public LayerList
{
public:
    Layer* myLayerPointer() override
    {
        Q_ASSERT_X(false, "LayerSet::myLayerPointer", "Attempt to cast from LayerSet to Layer");
        throw;
    }
    LayerSet* myLayerSetPointer() override
    {
        return this;
    }
    bool isLayerSet() const override
    {
        return true;
    }

    //see layerset.cpp
    RenderTree compileRenderTree();
};

#endif // LAYERSET_H
