#ifndef LAYER_H
#define LAYER_H

//Qt
#include <QImage>
#include <QColor>

//Own
#include "abstractlayer.h"
#include "layerimage.h"

class Layer : public AbstractLayer
{
public:
    Layer() : img(nullptr)
    {

    }

    Layer(LayerImage *image)
    {
        setImage(image);
    }

    Layer(LayerImage* image, BlendFunction blendMode)
    {
        setImage(image);
        setBlendMode(blendMode);
    }

    Layer* myLayerPointer() override
    {
        return this;
    }
    LayerSet* myLayerSetPointer() override
    {
        Q_ASSERT_X(false, "Layer::myLayerSetPointer", "Attempt to cast from Layer to LayerSet");
        throw;
    }
    bool isLayer() const override   { return true; }

    inline void setImage(LayerImage* image) { img = image; }
    inline LayerImage* image() const        { return img;  }

protected:
    LayerImage* img;

};

#endif // LAYER_H
