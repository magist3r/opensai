#ifndef BLENDMODE_H
#define BLENDMODE_H

//Qt
#include <QColor>

typedef int(*BlendFunction)(int /*backdrop*/, int /*source*/);

inline int blendAlpha(const int as, const int ab)
{
    return as + (ab*(0xFF - as))/0xFF;
}

inline int composite(const int cs, const int cb, const int as, int ab, const BlendFunction blend)
{
    return ((as*((0xFF-ab)*cs + ab * blend(cb, cs)) + (0xFF - as)*ab*cb))/0xFF;
}

QRgb applyForRgb(const QRgb source, const QRgb backdrop, const BlendFunction blend);

//Blend modes

int normal(int cb, int cs);
int multiply(int cb, int cs);
int darken(int cb, int cs);
int screen(int cb, int cs);
int overlay(int cb, int cs);



#endif // BLENDMODE_H
