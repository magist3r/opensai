//Qt
#include <QImage>
#include <QDebug>
#include <QTime>

//Own
#include "layertest.h"
#include "layer.h"
#include "layerset.h"
#include "rendertree.h"

void layerTest()
{
    int w = 250;
    int h = 250;
    QSize size(w, h);
    LayerImage defaultImage(size.width(), size.height());
    defaultImage.fill(Qt::transparent);

    LayerImage ponyImage(QImage("pony.png").convertToFormat(QImage::Format_ARGB32));
    LayerImage scribbleImage(QImage("scribble.png").convertToFormat(QImage::Format_ARGB32));

    if (defaultImage.isNull() || ponyImage.isNull() || scribbleImage.isNull())
    {
        qDebug() << "Can't load images";
        return;
    }

    if (ponyImage.size() != size || scribbleImage.size() != size)
    {
        qDebug() << "Invalid sizes";
        return;
    }

    LayerSet layerSet;

    Layer* defaultLayer = new Layer(&defaultImage);
    Layer* pony = new Layer(&ponyImage, normal);
    Layer* scribble = new Layer(&scribbleImage, multiply);

    layerSet.append(defaultLayer);
    layerSet.append(pony);
    layerSet.append(scribble);



    QImage result;

    QTime time;
    time.start();
    RenderTree renderTree = layerSet.compileRenderTree();

    result = renderImage(renderTree, w, h);

    /*for (int i=0; i<h; ++i)
    {
        for (int j=0; j<w; ++j)
        {
            result.setPixel(i, j, renderPixel(renderTree, i, j));
        }
    }*/

    qDebug("Time elapsed: %d ms", time.elapsed());

    result.save(QString("result.png"));

    delete defaultLayer;
    delete pony;
    delete scribble;
}
