//Own
#include "blendmode.h"

QRgb applyForRgb(const QRgb source, const QRgb backdrop, const BlendFunction blend)
{
    const int as = qAlpha(source);
    const int ab = qAlpha(backdrop);

    const int alpha = blendAlpha(as, ab);

    if (alpha)
    {
        const int red   = composite(qRed(source),   qRed(backdrop),   as, ab, blend) / alpha;
        const int green = composite(qGreen(source), qGreen(backdrop), as, ab, blend) / alpha;
        const int blue  = composite(qBlue(source),  qBlue(backdrop),  as, ab, blend) / alpha;

        return qRgba(red, green, blue, alpha);
    }
    else return 0u; //transparent
}

int multiply(int cb, int cs)
{
    return cb*cs/0xFF;
}

int normal(int cb, int cs)
{
    Q_UNUSED(cb);
    return cs;
}

int darken(int cb, int cs)
{
    return (cb > cs)? cs : cb;
}

int screen(int cb, int cs)
{
    return (cb + cs - (cb * cs))/0xFF;
}

int overlay(int cb, int cs)
{
    if(cs <= 127)
        return multiply(cb, 2 * cs);
    else
        return screen(cb, 2 * cs - 0xFF);
}
