#-------------------------------------------------
#
# Project created by QtCreator 2013-04-25T19:21:34
#
#-------------------------------------------------

QT       -= core gui

TARGET = partyblendmodes
TEMPLATE = lib

SOURCES += \
    partyblendmodes.cpp

HEADERS += \
    partyblendmodes.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
