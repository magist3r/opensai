#include "partyblendmodes.h"


int color_dodge(int cb, int cs)
{
    if (cb == 0) {
        return 0;
    }
    else if (cs == 0xFF) {
        return 0xFF;
    }
    else {
        int val = (cb << 8)/(0xFF - cs);
        return 0xFF < val ? 0xFF : val;
    }
}

int color_burn(int cb, int cs)
{
    if (cb == 0xFF) {
        return 0;
    }
    else if (cs == 0) {
        return 0xFF;
    }
    else{
        int val = ((0xFF - cb) << 8)/cs;
        return 0xFF - (0xFF < val ? 0xFF : val);
    }
}

int difference(int cb, int cs)
{
    return ((cb - cs) > 0) ? (cb - cs) : (cs - cb);
}

int exclusion(int cb, int cs)
{
    return (((cb + cs) << 8) - (cb*cs << 1)) >> 8;
}

const char** modeNames()
{
    static const char* names[] = { "color_dodge", "color_burn", "difference", "exclusion", 0 };
    return names;
}
