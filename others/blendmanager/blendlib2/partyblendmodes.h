#ifndef DEFAULTBLENDMODES_H
#define DEFAULTBLENDMODES_H

#ifdef __cplusplus
extern "C"
{
#endif

extern const char** modeNames();

extern int color_dodge(int cb, int cs);
extern int color_burn(int cb, int cs);
extern int difference(int cb, int cs);
extern int exclusion(int cb, int cs);

#ifdef __cplusplus
}
#endif

#endif // DEFAULTBLENDMODES_H
