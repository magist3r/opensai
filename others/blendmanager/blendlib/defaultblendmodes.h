#ifndef DEFAULTBLENDMODES_H
#define DEFAULTBLENDMODES_H

#ifdef __cplusplus
extern "C"
{
#endif

extern const char** modeNames();

extern int multiply(int cb, int cs);
extern int darken(int cb, int cs);
extern int lighten(int cb, int cs);
extern int screen(int cb, int cs);
extern int overlay(int cb, int cs);

#ifdef __cplusplus
}
#endif

#endif // DEFAULTBLENDMODES_H
