#include "defaultblendmodes.h"



int multiply(int cb, int cs)
{
    return cb*cs >> 8;
}

int darken(int cb, int cs)
{
    return (cb > cs)? cs : cb;
}

int lighten(int cb, int cs)
{
    return (cb < cs)? cs : cb;
}

int screen(int cb, int cs)
{
    return (cb + cs - (cb * cs)) >> 8;
}

int overlay(int cb, int cs)
{
    if(cs <= 0x7F)
        return multiply(cb, 2 * cs);
    else
        return screen(cb, 2 * cs - 0xFF);
}

const char** modeNames()
{
    static const char* names[] = { "multiply", "screen", "overlay", "darken", "lighten", 0 };
    return names;
}
