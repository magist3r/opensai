#include "blendmapper.h"

int normal(int cb, int cs)
{
    Q_UNUSED(cb);
    return cs;
}


BlendModeInfo::BlendModeInfo() : _func(nullptr)
{

}

BlendModeInfo::~BlendModeInfo() {}

BlendModeInfo::BlendModeInfo(QString libFileName, QString originalName, QString alias, BlendFunction blendMode) :
    _libFileName(libFileName), _originalName(originalName), _alias(alias), _func(blendMode)
{

}

BlendModeInfo::BlendModeInfo(const BlendModeInfo& other) :
    _libFileName(other.libFileName()), _originalName(other.originalName()), _alias(other.alias()), _func(other.blendFunction())
{

}

BlendModeInfo& BlendModeInfo::operator=(const BlendModeInfo& other)
{
    _libFileName = other.libFileName();
    _originalName = other.originalName();
    _alias = other.alias();
    _func = other.blendFunction();

    return *this;
}

bool BlendModeInfo::operator==(const BlendModeInfo& other) const
{
    return (originalName() == other.originalName() && libFileName() == other.libFileName());
}

QString BlendModeInfo::libFileName() const {
    return _libFileName;
}
QString BlendModeInfo::originalName() const {
    return _originalName;
}
QString BlendModeInfo::alias() const {
    return _alias;
}
BlendFunction BlendModeInfo::blendFunction() const {
    return _func;
}

void BlendModeInfo::setAlias(QString alias) {
    _alias = alias;
}


BlendMapper::BlendMapper()
{
    _blendList.append(BlendModeInfo("", "normal", "Normal", normal));
}

BlendMapper* BlendMapper::globalInstance()
{
    static BlendMapper blendMapper;
    return &blendMapper;
}
