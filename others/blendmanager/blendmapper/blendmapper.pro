#-------------------------------------------------
#
# Project created by QtCreator 2013-04-28T12:47:21
#
#-------------------------------------------------

QT       += core gui

TARGET = blendmapper

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    blendmapper.cpp \
    widget.cpp \
    blendmanager.cpp

HEADERS += \
    blendmapper.h \
    widget.h \
    blendmanager.h
