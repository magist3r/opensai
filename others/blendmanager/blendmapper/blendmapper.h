#ifndef BLENDMAPPER_H
#define BLENDMAPPER_H

#include <QObject>
#include <QPair>
#include <QString>
#include <QList>
#include <QMetaType>

typedef int(*BlendFunction)(int /*backdrop*/, int /*source*/);

int normal(int cb, int cs);

struct BlendModeInfo
{
    BlendModeInfo();
    ~BlendModeInfo();
    BlendModeInfo(QString libFileName, QString originalName, QString alias, BlendFunction blendMode);
    BlendModeInfo(const BlendModeInfo& other);
    BlendModeInfo& operator=(const BlendModeInfo& other);

    bool operator==(const BlendModeInfo& other) const;

    QString libFileName() const;
    QString originalName() const;
    QString alias() const;
    BlendFunction blendFunction() const;

    void setAlias(QString alias);

private:
    QString _libFileName;
    QString _originalName;
    QString _alias;
    BlendFunction _func;
};


Q_DECLARE_METATYPE(BlendModeInfo)

class BlendMapper
{
public:
    explicit BlendMapper();

    explicit BlendMapper(const BlendMapper& other) : _blendList (other.blendModeList())
    {

    }

    static BlendMapper* globalInstance();

    QList<BlendModeInfo> blendModeList() const {
        return _blendList;
    }
    void setBlendModeList(QList<BlendModeInfo> blendList)
    {
        _blendList.erase(_blendList.begin()+1, _blendList.end()); //erase all except Normal mode
        _blendList.append(blendList);
    }

    bool contains(BlendModeInfo blendInfo)
    {
        return _blendList.contains(blendInfo);
    }

    bool containsByAlias(QString alias)
    {
        bool b = false;
        foreach(BlendModeInfo blendInfo, blendModeList())
        {
            if (blendInfo.alias() == alias)
            {
                b = true;
                break;
            }
        }
        return b;
    }
    
private:
    QList<BlendModeInfo> _blendList;
};


#endif // BLENDMAPPER_H
