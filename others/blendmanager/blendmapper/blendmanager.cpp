//Own
#include "blendmanager.h"
#include "blendmapper.h"

//Qt
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStandardItemModel>
#include <QTreeView>
#include <QTextEdit>
#include <QListView>
#include <QVariant>
#include <QDir>
#include <QDebug>
#include <QLibrary>
#include <QModelIndex>

BlendManager::BlendManager(QWidget *parent) :
    QDialog(parent)
{
    setupUi();
    loadFunctions();
}

void BlendManager::setupUi()
{
    //buttons for layout editing
    up = new QPushButton("Up");
    down = new QPushButton("Down");

    connect(up, SIGNAL(clicked()), this, SLOT(upSelected()));
    connect(down, SIGNAL(clicked()), this, SLOT(downSelected()));

    //list of currently used blendmodes
    listModel = new QStandardItemModel(this);
    connect(listModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(updateAlias(QStandardItem*)));
    QList<BlendModeInfo> blendModes = BlendMapper::globalInstance()->blendModeList();
    blendModes.pop_front(); //remove Normal mode
    foreach(BlendModeInfo blendInfo, blendModes)
    {
        QVariant variant;
        variant.setValue(blendInfo);
        QStandardItem* item = new QStandardItem(blendInfo.alias());
        item->setData(variant);
        listModel->appendRow(item);
    }

    listView = new QListView(this);
    listView->setSelectionMode(QAbstractItemView::SingleSelection);
    listView->setModel(listModel);

    connect(listView, SIGNAL(clicked(QModelIndex)), this, SLOT(showInfo(QModelIndex)));

    //show blendmodeinfo
    blendText = new QTextEdit();
    blendText->setReadOnly(true);

    //tree of available blendmodes, found in libraries
    treeModel = new QStandardItemModel(this);
    treeModel->setHeaderData(0, Qt::Horizontal, QString("tree"));
    treeView = new QTreeView(this);
    treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    treeView->setModel(treeModel);

    //console for error output
    console = new QTextEdit();
    console->setReadOnly(true);

    //add/remove blendmodes
    left = new QPushButton("<");
    right = new QPushButton(">");

    connect(left, SIGNAL(clicked()), this, SLOT(moveFromTreeToList()));
    connect(right, SIGNAL(clicked()), this, SLOT(moveFromListToTree()));

    //standard dialog buttons
    okCmd = new QPushButton(tr("OK"));
    cancelCmd = new QPushButton(tr("Cancel"));
    applyCmd = new QPushButton(tr("Apply"));

    connect(okCmd, SIGNAL(clicked()), this, SLOT(saveBlendInfo()));
    connect(applyCmd, SIGNAL(clicked()), this, SLOT(saveBlendInfo()));

    connect(okCmd, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelCmd, SIGNAL(clicked()), this, SLOT(reject()));

    //layouts
    QVBoxLayout* upDownBox = new QVBoxLayout();
    upDownBox->addStretch();
    upDownBox->addWidget(up,0,Qt::AlignHCenter);
    upDownBox->addWidget(down,0,Qt::AlignHCenter);
    upDownBox->addStretch();

    QHBoxLayout* hbox1 = new QHBoxLayout();
    hbox1->addLayout(upDownBox);
    hbox1->addWidget(listView);
    hbox1->addWidget(left,0,Qt::AlignHCenter);
    hbox1->addWidget(right,0,Qt::AlignHCenter);
    hbox1->addWidget(treeView);

    QHBoxLayout* hbox2 = new QHBoxLayout();
    hbox2->addWidget(blendText);
    hbox2->addWidget(console);

    QHBoxLayout* cmdBox = new QHBoxLayout();
    cmdBox->addStretch();
    cmdBox->addWidget(okCmd);
    cmdBox->addWidget(cancelCmd);
    cmdBox->addWidget(applyCmd);

    QVBoxLayout* vbox = new QVBoxLayout();
    vbox->addLayout(hbox1);
    vbox->addLayout(hbox2);
    vbox->addLayout(cmdBox);


    setLayout(vbox);
}

void BlendManager::loadFunctions()
{
    QString dirname("blendlibs");
    QDir libdir;
    if (!libdir.cd(dirname))
    {
        atConsole(tr("Can't enter into directory '%0'").arg(dirname), ErrorMessage);
    }
    else
    {
        int libcount = 0;
        foreach(QString libname, libdir.entryList(QDir::Files|QDir::NoSymLinks|QDir::NoDotAndDotDot,
                                                  QDir::Name))
        {
            if (QLibrary::isLibrary(libname))
            {
                libcount++;
                QLibrary lib(libdir.absoluteFilePath(libname));
                lib.load();
                if (lib.isLoaded())
                {
                    atConsole(tr("%0 loaded").arg(libname));
                    typedef const char**(*ModeNames)();
                    typedef int (*BlendFunction)(int, int);

                    ModeNames getModeNames = ModeNames(lib.resolve("modeNames"));
                    if (getModeNames)
                    {
                        QStandardItem* rootItem = treeModel->invisibleRootItem();
                        QStandardItem* libItem = new QStandardItem(libname);
                        libItem->setEditable(false);
                        libItem->setToolTip(tr("Library file name"));
                        rootItem->appendRow(libItem);
                        const char** modeNames = getModeNames();
                        while(*modeNames)
                        {
                            const char* modeName = *modeNames;
                            QString modeString = QString(modeName);
                            modeNames++;

                            BlendFunction blendFunc = BlendFunction(lib.resolve(modeName));
                            if (blendFunc)
                            {
                                atConsole(tr("%0 resolved").arg(modeString));

                                QString aliasString = toAlias(modeString);
                                BlendModeInfo blendInfo(libname, modeString, aliasString, blendFunc);

                                if (!BlendMapper::globalInstance()->contains(blendInfo))
                                {
                                    QStandardItem* funcItem = new QStandardItem(modeString);
                                    funcItem->setEditable(false);
                                    funcItem->setToolTip(tr("Blendmode name as defined by library"));
                                    QVariant variant;
                                    variant.setValue(blendInfo);
                                    funcItem->setData(variant, DataRole);
                                    libItem->appendRow(funcItem);
                                }
                                else
                                {
                                    atConsole(tr("%0 from %1 is already in global blend mapper")
                                              .arg(blendInfo.originalName(), blendInfo.libFileName()));
                                }

                            }
                            else
                            {
                                atConsole(tr("Can't resolve symbol %0 from %1. Last error: %2")
                                                .arg(modeString, libname, lib.errorString()), ErrorMessage);
                            }

                        }
                    }
                    else
                    {
                        atConsole(tr("Can't resolve modeNames function from %0. Last error: %1")
                                        .arg(libname, lib.errorString()), ErrorMessage);
                    }
                }
                else
                {
                    atConsole(tr("Can't load library %0. Last error: %1")
                                    .arg(libname, lib.errorString()), ErrorMessage);
                }
            }
        }

        if (libcount == 0)
        {
            atConsole(tr("No libraries found. Check '%0' directory").arg(dirname));
        }
    }
}

void BlendManager::moveFromTreeToList()
{
    QModelIndexList indexList = treeView->selectionModel()->selection().indexes();

    foreach(QModelIndex index, indexList)
    {
        QVariant variant = index.data(DataRole);
        if (variant.isValid() && variant.canConvert<BlendModeInfo>())
        {
            BlendModeInfo blendInfo = variant.value<BlendModeInfo>();

            QList<QStandardItem*> itemList = listModel->findItems(blendInfo.alias());
            if (itemList.isEmpty())
            {
                treeModel->removeRow(index.row(), index.parent());

                QStandardItem* item = new QStandardItem(blendInfo.alias());
                QVariant var;
                var.setValue(blendInfo);
                item->setData(var, DataRole);
                listModel->appendRow(item);
            }
            else
            {
                atConsole(tr("Blendmode with name %0 already exists").arg(blendInfo.alias()), ErrorMessage);
            }

        }
    }
}

void BlendManager::moveFromListToTree()
{
    QModelIndexList indexList = listView->selectionModel()->selection().indexes();

    foreach(QModelIndex index, indexList)
    {
        QVariant variant = index.data(DataRole);
        if (variant.isValid() && variant.canConvert<BlendModeInfo>())
        {
            BlendModeInfo blendInfo = variant.value<BlendModeInfo>();
            listModel->removeRow(index.row(), index.parent());

            QList<QStandardItem*> itemList = treeModel->findItems(blendInfo.libFileName());
            if (itemList.isEmpty())
            {
                atConsole(tr("No such library %0").arg(blendInfo.libFileName()), ErrorMessage);
            }
            else
            {
                QStandardItem* item = new QStandardItem(blendInfo.originalName());
                QVariant var;
                var.setValue(blendInfo);
                item->setData(var, DataRole);
                QStandardItem* parent = itemList[0];
                parent->appendRow(item);
            }

        }
    }

    indexList = listView->selectionModel()->selection().indexes();

    if (!indexList.isEmpty())
    {
        showInfo(indexList[0]);
    }
}

QString BlendManager::toAlias(QString name)
{
    QString alias = name.left(1).toUpper() + name.mid(1);
    alias.replace("_", "-");
    return alias;
}

void BlendManager::atConsole(QString str, MessageType msg)
{
    if (msg == ErrorMessage)
    {
        console->append("<font color=red>" + str + "</font>");
    }
    else
    {
        console->append(str);
    }
}

void BlendManager::showInfo(const QModelIndex &index)
{
    if (index.isValid() && index.model() == listModel)
    {
        QStandardItem* item = listModel->itemFromIndex(index);
        QVariant variant = item->data(DataRole);
        if (variant.isValid() && variant.canConvert<BlendModeInfo>())
        {
            BlendModeInfo blendInfo = variant.value<BlendModeInfo>();
            blendText->setText(tr("Library file name: %0\nOriginal function name: %1\nAlias: %2")
                               .arg(blendInfo.libFileName(), blendInfo.originalName(), blendInfo.alias()));
        }
    }
}

void BlendManager::saveBlendInfo()
{
    QList<BlendModeInfo> list;
    for(int i=0; i<listModel->rowCount(); ++i)
    {
        QStandardItem* item = listModel->item(i);
        if (item)
        {
            QVariant variant = item->data(DataRole);
            if (variant.isValid() && variant.canConvert<BlendModeInfo>())
            {
                BlendModeInfo blendInfo = variant.value<BlendModeInfo>();
                list.append(blendInfo);
            }
        }
    }
    BlendMapper::globalInstance()->setBlendModeList(list);
}

void BlendManager::updateAlias(QStandardItem *item)
{
    QString alias = item->data(Qt::DisplayRole).toString();
    QVariant variant = item->data(DataRole);
    if (variant.isValid() && variant.canConvert<BlendModeInfo>())
    {
        BlendModeInfo blendInfo = variant.value<BlendModeInfo>();
        if (alias != blendInfo.alias())
        {
            blendInfo.setAlias(alias);
            QVariant var;
            var.setValue(blendInfo);
            item->setData(var, DataRole);
        }
    }
}

void BlendManager::upSelected()
{
    QModelIndexList indexList = listView->selectionModel()->selection().indexes();

    if (!indexList.isEmpty())
    {
        QModelIndex index = indexList[0];
        if (index.row() > 0)
        {
            QModelIndex prev = listModel->index(index.row()-1, 0, index.parent());

            if (prev.isValid())
            {
                auto data = listModel->itemData(prev);
                listModel->setItemData(prev, listModel->itemData(index));
                listModel->setItemData(index, data);
                listView->setCurrentIndex(prev);
            }
        }
    }
}

void BlendManager::downSelected()
{
    QModelIndexList indexList = listView->selectionModel()->selection().indexes();

    if (!indexList.isEmpty())
    {
        QModelIndex index = indexList[0];
        if (index.row() < listModel->rowCount())
        {
            QModelIndex next = listModel->index(index.row()+1, 0, index.parent());

            if (next.isValid())
            {
                auto data = listModel->itemData(next);
                listModel->setItemData(next, listModel->itemData(index));
                listModel->setItemData(index, data);
                listView->setCurrentIndex(next);
            }
        }
    }
}

