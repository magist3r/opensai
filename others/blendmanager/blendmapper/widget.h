#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QComboBox;
class QPushButton;

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    

signals:
    
public slots:
    void blendManager();
    
private:
    void updateComboBox();

    QComboBox* comboBox;
    QPushButton* button;
};

#endif // WIDGET_H
