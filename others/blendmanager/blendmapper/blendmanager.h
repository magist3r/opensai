#ifndef BLENDMANAGER_H
#define BLENDMANAGER_H

//Qt
#include <QDialog>
#include <QModelIndex>

class QPushButton;
class QStandardItemModel;
class QTreeView;
class QTextEdit;
class QListView;
class QStandardItem;

class BlendManager : public QDialog
{
    Q_OBJECT
public:
    enum {DataRole = Qt::UserRole + 1};
    enum MessageType {NormalMessage, ErrorMessage};

    explicit BlendManager(QWidget *parent = 0);
    
signals:
    
public slots:
    void moveFromTreeToList();
    void moveFromListToTree();

    void atConsole(QString str, MessageType msg = NormalMessage);
    void showInfo(const QModelIndex& index);

    void saveBlendInfo();
    void updateAlias(QStandardItem* item);

    void upSelected();
    void downSelected();
    
private:

    void setupUi();
    void loadFunctions();

    QString toAlias(QString name);


    QStandardItemModel* listModel;
    QStandardItemModel* treeModel;

    QListView* listView;
    QTreeView* treeView;

    QTextEdit* blendText;
    QTextEdit* console;

    QPushButton* up;
    QPushButton* down;
    QPushButton* left;
    QPushButton* right;

    QPushButton* okCmd;
    QPushButton* cancelCmd;
    QPushButton* applyCmd;
};

#endif // BLENDMANAGER_H
