//Own
#include "widget.h"
#include "blendmanager.h"
#include "blendmapper.h"

//Qt
#include <QComboBox>
#include <QPushButton>
#include <QVBoxLayout>

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    comboBox = new QComboBox();
    updateComboBox();
    button = new QPushButton(tr("Blend Mode Manager"));

    connect(button, SIGNAL(clicked()), this, SLOT(blendManager()));

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addWidget(comboBox);
    vbox->addWidget(button);

    setLayout(vbox);
}

void Widget::blendManager()
{
    BlendManager* dialog = new BlendManager();
    dialog->exec();
    delete dialog;
    updateComboBox();
}

void Widget::updateComboBox()
{
    comboBox->clear();
    foreach(BlendModeInfo blendInfo, BlendMapper::globalInstance()->blendModeList())
    {
        QVariant variant;
        variant.setValue(blendInfo);
        comboBox->addItem(blendInfo.alias(), variant);
    }
}
