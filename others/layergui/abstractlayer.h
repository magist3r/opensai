#ifndef ABSTRACTLAYER_H
#define ABSTRACTLAYER_H

//Own
#include "blendmode.h"

class Layer;
class LayerSet;



class AbstractLayer
{
public:
    AbstractLayer();
    virtual ~AbstractLayer() {}

    inline void setBlendMode(Blending::BlendFunction blendMode)  { blendFunc = blendMode; }
    inline Blending::BlendFunction blendMode() const             { return blendFunc;      }

    inline void setActive(bool a)  { active = a;    }
    inline bool isActive() const   { return active; }

    virtual Layer* myLayerPointer() = 0;
    virtual LayerSet* myLayerSetPointer() = 0;
    virtual bool isLayer() const   { return false; }
    virtual bool isLayerSet() const{ return false; }

protected:
    Blending::BlendFunction blendFunc;
    bool active;
};

#endif // ABSTRACTLAYER_H
