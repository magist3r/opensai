//Qt
#include <QDebug>

//Own
#include "layerset.h"

RenderTree LayerSet::compileRenderTree()
{
    RenderTree renderTree;

    for (auto iter = this->begin(); iter != this->end(); ++iter)
    {
        AbstractLayer* currentLayer = *iter;

        if (currentLayer->isActive())
        {
            if (currentLayer->isLayer())
            {
                renderTree.push_back(RenderNode(currentLayer));
            }
            else if (currentLayer->isLayerSet())
            {
                LayerSet* layerSet = currentLayer->myLayerSetPointer();
                RenderTree branch = layerSet->compileRenderTree();
                if (!branch.isEmpty())
                   renderTree.push_back(RenderNode(layerSet, branch));
            }
            else Q_ASSERT_X(false, "compileRenderTree", "Unknown type of layer");
        }
    }

    return renderTree;
}
