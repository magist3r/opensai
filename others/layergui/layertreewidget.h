#ifndef LAYERTREEWIDGET_H
#define LAYERTREEWIDGET_H

//Qt
#include <QImage>
#include <QList>
#include <QPixmap>
#include <QSize>
#include <QString>
#include <QWidget>

class LayerSet;
class LayerFrame;
class QSpacerItem;
class QVBoxLayout;

class LayerTreeWidget : public QWidget
{
    Q_OBJECT
public:

    struct Memento
    {
        friend class LayerTreeWidget;
    private:
        QSize layerSize;
        QList<LayerFrame*> upperLevelLayers;
        LayerFrame* selected;

        int layerCounter;
        int setCounter;
    };

    explicit LayerTreeWidget(QSize laySize,QWidget *parent = 0);

    Memento toMemento();
    void fromMemento(const Memento& memento);

    void insertLayerAfter(LayerFrame* toInsert, LayerFrame* other);
    void insertLayerBefore(LayerFrame* toInsert, LayerFrame* other);
    void insertLayerIntoSet(LayerFrame* toInsert, LayerFrame* set);

    QSize layerSize() const {
        return _layerSize;
    }

signals:
    void debugInfoUpdated(QString);
    void imageRendered(QPixmap);

    void blendModeChanged(QString blendMode);
    
public slots:
    void updateDebugInfo();
    void renderImage();

    void addNewLayer();
    void addNewLayerSet();
    void removeSelected();

    void wasSelected();
    void setSelected(LayerFrame *layerFrame);

    void setBlendModeForSelected(QString blendMode);

private:
    void detachLayer(LayerFrame* layerFrame);
    void reconstructVBox();
    void addLayerImpl(LayerFrame *layerFrame);

    void insertUpperLayer(LayerFrame* layerFrame, int ind);
    void removeUpperLayer(LayerFrame* layerFrame);

    QList<LayerFrame*> getAllLayerFrames();

    QVBoxLayout* _vbox;
    LayerFrame* _selected; //current selected LayerFrame

    int _layerCounter;
    int _setCounter;

    QList<LayerFrame*> _upperLevelLayers;

    QSize _layerSize;
    LayerSet* _upperLayerSet;
};

#endif // LAYERTREEWIDGET_H
