//Qt
#include <QRgb>

//Own
#include "rendertree.h"
#include "layer.h"


QRgb Rendering::renderPixel(const RenderTree &renderTree, int x, int y)
{
    using namespace Blending;
    Q_ASSERT_X(!renderTree.isEmpty(), "renderPixel", "Can't render empty RenderTree");
    QRgb rgb;

    RenderNode lastNode = renderTree.last();

    if (lastNode.layer->isLayer())
    {
        rgb = lastNode.layer->myLayerPointer()->image().pixel(x, y);
    }
    else //(lastNode.layer->isLayerSet())
    {
        rgb = renderPixel(lastNode.branchSet, x, y);
    }

    for (int i = renderTree.size()-1; i>=1; --i)
    {
        BlendFunction blendMode = renderTree.at(i).layer->blendMode();
        RenderNode renderNode = renderTree.at(i-1);
        QRgb backdrop;

        if (renderNode.layer->isLayer())
        {
            backdrop = renderNode.layer->myLayerPointer()->image().pixel(x, y);
        }
        else //(renderNode.layer->isLayerSet())
        {
            backdrop = renderPixel(renderNode.branchSet, x, y);
        }

        rgb = applyForRgb(rgb, backdrop, blendMode);
    }

    return rgb;
}

QImage Rendering::renderImage(const RenderTree &renderTree, const int w, const int h)
{
    using namespace Blending;
    QImage result;
    RenderNode lastNode = renderTree.last();

    if (lastNode.layer->isLayer())
    {
        result = lastNode.layer->myLayerPointer()->image();
    }
    else //(lastNode.layer->isLayerSet())
    {
        result = renderImage(lastNode.branchSet, w, h);
    }

    for (int i = renderTree.size()-1; i>=1; --i)
    {
        BlendFunction blendMode = renderTree.at(i).layer->blendMode();
        RenderNode renderNode = renderTree.at(i-1);
        QImage backdrop;

        if (renderNode.layer->isLayer())
        {
            backdrop = renderNode.layer->myLayerPointer()->image();
        }
        else //(renderNode.layer->isLayerSet())
        {
            backdrop = renderImage(renderNode.branchSet, w, h);
        }

        for (int i=0; i<h; ++i)
        {
            const QRgb* backLine = reinterpret_cast<const QRgb*>(backdrop.scanLine(i));
            QRgb* resultLine = reinterpret_cast<QRgb*>(result.scanLine(i));

            for (int j=0; j<w; ++j)
            {
                resultLine[j] = applyForRgb(resultLine[j], backLine[j], blendMode);
            }
        }
    }
    return result;
}
