#-------------------------------------------------
#
# Project created by QtCreator 2013-04-08T18:56:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++0x

TARGET = layergui
TEMPLATE = app


SOURCES += main.cpp \
    layerframe.cpp \
    layertreewidget.cpp \
    layerframemime.cpp \
    abstractlayer.cpp \
    blendmode.cpp \
    rendertree.cpp \
    layerset.cpp \
    layerimage.cpp

HEADERS  += \
    layerframe.h \
    layertreewidget.h \
    layerframemime.h \
    abstractlayer.h \
    blendmode.h \
    layer.h \
    rendertree.h \
    layerset.h \
    layerimage.h

FORMS    +=
