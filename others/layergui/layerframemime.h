#ifndef LAYERFRAMEMIME_H
#define LAYERFRAMEMIME_H

#include <QDrag>
#include <QMimeData>
#include "layerframe.h"

class LayerFrameMime : public QMimeData
{
public:
    explicit LayerFrameMime();

    static QString mimeType()
    {
        return "application/layerframe";
    }

    void setLayerFrame(LayerFrame* layerFrame);

    LayerFrame* layerFrame() const
    {
        return _layerFrame;
    }
    
private:
    LayerFrame* _layerFrame;
    
};

class LayerFrameDrag : public QDrag
{
public:
    LayerFrameDrag(QWidget* dragSource = 0) : QDrag(dragSource)
    {

    }

    void setLayerFrame(LayerFrame* layerFrame);
};

#endif // LAYERFRAMEMIME_H
