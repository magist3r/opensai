//Own
#include "blendmode.h"

QRgb Blending::applyForRgb(const QRgb source, const QRgb backdrop, const BlendFunction blend)
{
    const int as = qAlpha(source);
    const int ab = qAlpha(backdrop);

    const int alpha = blendAlpha(as, ab);

    if (alpha)
    {
        const int red   = composite(qRed(source),   qRed(backdrop),   as, ab, blend) / alpha;
        const int green = composite(qGreen(source), qGreen(backdrop), as, ab, blend) / alpha;
        const int blue  = composite(qBlue(source),  qBlue(backdrop),  as, ab, blend) / alpha;

        return qRgba(red, green, blue, alpha);
    }
    else return 0u; //transparent
}

int Blending::multiply(int cb, int cs)
{
    return cb*cs/0xFF;
}

int Blending::normal(int cb, int cs)
{
    Q_UNUSED(cb);
    return cs;
}

int Blending::darken(int cb, int cs)
{
    return (cb > cs)? cs : cb;
}

int Blending::screen(int cb, int cs)
{
    return (cb + cs - (cb * cs))/0xFF;
}

int Blending::overlay(int cb, int cs)
{
    if(cs <= 127)
        return multiply(cb, 2 * cs);
    else
        return screen(cb, 2 * cs - 0xFF);
}

static QString names[] =       {"Normal", "Multiply", "Darken", "Screen", "Overlay"};
static Blending::BlendFunction modes[] = { Blending::normal,
                                           Blending::multiply,
                                           Blending::darken,
                                           Blending::screen,
                                           Blending::overlay
                                         };

Blending::BlendFunction Blending::blendModeFromName(QString name)
{
    for (size_t i=0; i<sizeof(names)/sizeof(QString); ++i)
    {
        if (names[i] == name)
            return modes[i];
    }

    return nullptr;
}

QString Blending::nameOfBlendMode(BlendFunction blendMode)
{
    for (size_t i=0; i<sizeof(modes)/sizeof(BlendFunction); ++i)
    {
        if (modes[i] == blendMode)
            return names[i];
    }

    return QString();
}
