#ifndef LAYERIMAGE_H
#define LAYERIMAGE_H

//Qt
#include <QBitArray>
#include <QImage>

class LayerImage : public QImage
{
public:
    LayerImage(int w, int h);
    LayerImage(const QImage& img);
    LayerImage(const LayerImage& li);

    bool hasColorAt(int x, int y) const;
};

#endif // LAYERIMAGE_H
