//Own
#include "layerframe.h"
#include "layertreewidget.h"
#include "layerframemime.h"
#include "layer.h"
#include "layerset.h"
#include "blendmode.h"

//Qt
#include <QApplication>
#include <QBuffer>
#include <QByteArray>
#include <QBrush>
#include <QCheckBox>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QFileDialog>
#include <QFont>
#include <QHBoxLayout>
#include <QMenu>
#include <QLabel>
#include <QList>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QPoint>
#include <QStack>
#include <QVBoxLayout>

//==================
// MarkerEventFilter
//==================
MarkerEventFilter::MarkerEventFilter(QObject *parent) : QObject(parent)
{

}

bool MarkerEventFilter::eventFilter(QObject *obj, QEvent *e)
{
    if (   e->type() == QEvent::MouseButtonPress
        || e->type() == QEvent::MouseButtonRelease
        || e->type() == QEvent::MouseButtonDblClick )
    {
        QMouseEvent *me = static_cast<QMouseEvent*>(e);
        if (me->button() == Qt::LeftButton)
        {
            return true;
        }
    }
    return QObject::eventFilter(obj, e);
}

//==================
// LayerFrame
//==================
LayerFrame::LayerFrame(bool isset, LayerTreeWidget *tree) :
    QFrame(tree), _selected(false), _isset(isset), _layerSet(nullptr), _dropPlace(LayerFrame::NoPlace)
{
    Q_ASSERT_X(tree, "LayerFrame::ctor", "LayerTreeWidget can't be nullptr");

    setAcceptDrops(true);

    setFixedSize(upperWidth(), defaultHeight());
    setLineWidth(10);
    setFrameShape(QFrame::StyledPanel);
    setFrameShadow(QFrame::Plain);

    QImage image(tree->layerSize(), QImage::Format_ARGB32);
    if (isset)
    {
        _layer = new LayerSet();
        image.fill(Qt::yellow);
    }
    else
    {
        _layer = new Layer();
        image.fill(Qt::white);
    }



    eyeBox = new QCheckBox();
    eyeBox->setChecked(true);
    eyeBox->setToolTip(tr("Show / Hide layer"));
    connect(eyeBox, SIGNAL(clicked(bool)), this, SLOT(showLayer(bool)));

    MarkerEventFilter* filter = new MarkerEventFilter(this);
    markerBox = new QCheckBox();
    markerBox->installEventFilter(filter);
    markerBox->setToolTip(tr("Working layer marker"));

    thumbnailLabel = new QLabel();
    layerNameLabel = new QLabel();
    blendModeLabel = new QLabel();
    opacityLabel = new QLabel();

    thumbnailLabel->setToolTip(tr("Thumbnail"));
    setBlendMode("Normal");
    setOpacity(100);

    QList<QLabel*> textLabels;
    textLabels << layerNameLabel << blendModeLabel << opacityLabel;

    QFont font = this->font();
    font.setPixelSize(10);
    QVBoxLayout* textBox = new QVBoxLayout();
    textBox->setSpacing(0);
    textBox->addStretch(0);

    //apply same font and height for every label
    foreach(QLabel* label, textLabels)
    {
        label->setFont(font);
        label->setMaximumHeight(height()/4);
        textBox->addWidget(label);
    }

    QVBoxLayout* checkBoxes = new QVBoxLayout();
    checkBoxes->addWidget(eyeBox);
    checkBoxes->addWidget(markerBox);

    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->addLayout(checkBoxes);
    hbox->addWidget(thumbnailLabel);
    hbox->addLayout(textBox);
    hbox->addStretch(0);

    int margin = height()/16;
    hbox->setContentsMargins(margin, margin, margin, margin);

    int thumbnailSide = height()-4*margin;
    thumbnailLabel->setFixedSize(thumbnailSide, thumbnailSide);

    setLayerImage(image);

    _defaultPalette = palette();
    setAutoFillBackground(true);
    this->setLayout(hbox);

}

LayerFrame::~LayerFrame()
{
    delete _layer;
}

void LayerFrame::setThumbnail(QPixmap pixmap)
{
    pixmap = pixmap.scaled(thumbnailLabel->size(), Qt::KeepAspectRatio);
    QPainter painter(&pixmap);
    painter.setPen(QPen(QBrush(QColor(0,0,0)), 1.0));
    int x = pixmap.width()-1;
    int y = pixmap.height()-1;
    QPoint points[4] = {QPoint(0,0), QPoint(x, 0), QPoint(x,y), QPoint(0,y)};
    painter.drawPolygon(points, 4);
    thumbnailLabel->setPixmap(pixmap);
}

void LayerFrame::setThumbnail(QImage image)
{
    QPixmap pixmap = QPixmap::fromImage(image, Qt::ColorOnly);
    setThumbnail(pixmap);
}

void LayerFrame::setLayerImage(QImage image)
{
    LayerTreeWidget* tree = treeWidget();
    image = image.scaled(tree->layerSize());
    if (_layer->isLayer())
    {
        _layer->myLayerPointer()->setImage(image);
    }
    setThumbnail(image);
}

void LayerFrame::setLayerName(QString str)
{
    layerNameLabel->setText(str);
}

void LayerFrame::setBlendMode(QString str)
{
    blendModeLabel->setText(str);
    Blending::BlendFunction blendMode = Blending::blendModeFromName(str);
    Q_ASSERT_X(blendMode, "LayerFrame::setBlendMode", "can't find blend mode by name");
    _layer->setBlendMode(blendMode);
}

void LayerFrame::setOpacity(int opacity)
{
    Q_ASSERT_X(opacity >= 0 && opacity <= 100, "LayerFrame::setOpacity", "invalid value, opacity has range [0:100]%" );
    opacityLabel->setText(QString::number(opacity) + "%");
}

void LayerFrame::select()
{
    markerBox->setChecked(true);
    _selected = true;
    emit selected();
}

void LayerFrame::unselect()
{
    markerBox->setChecked(false);
    _selected = false;
    emit unselected();
}

void LayerFrame::mousePressEvent(QMouseEvent *pe)
{
    if (pe->button() == Qt::LeftButton)
    {
        _dragStartPos = pe->pos();
    }
    QFrame::mousePressEvent(pe);
}

void LayerFrame::mouseMoveEvent(QMouseEvent *pe)
{
    int distance = (pe->pos() - _dragStartPos).manhattanLength();
    if (distance > QApplication::startDragDistance())
    {
        startDrag();
    }
    QFrame::mouseMoveEvent(pe);
}

void LayerFrame::mouseReleaseEvent(QMouseEvent *pe)
{
    if (pe->button() == Qt::LeftButton && !isSelected() && QRegion(rect()).contains(pe->pos()))
    {
        select();

    }
    QFrame::mouseReleaseEvent(pe);
}

void LayerFrame::paintEvent(QPaintEvent *pe)
{
    if (_selected)
    {
        QPalette pal = _defaultPalette;
        pal.setColor(QPalette::Window, QColor(32, 0, 196, 128));
        setPalette(pal);
    }
    else
    {
        setPalette(_defaultPalette);
    }
    QFrame::paintEvent(pe);
    if (_dropPlace != LayerFrame::NoPlace)
    {
        QPainter painter(this);
        painter.setPen(QPen(QBrush(Qt::red), 2.0));
        switch(_dropPlace)
        {
        case LayerFrame::TopLine:
            painter.drawLine(QPoint(1,1), QPoint(width()-1, 1));
            break;
        case LayerFrame::BottomLine:
            painter.drawLine(QPoint(1,height()-1), QPoint(width()-1, height()-1));
            break;
        case LayerFrame::RectBoard:
        {
            QPoint points[4] = {QPoint(1,1), QPoint(width()-1, 1), QPoint(width()-1, height()-1), QPoint(1,height()-1)};
            painter.drawPolygon(points, 4);
        }
            break;
        default:
            Q_ASSERT_X(false, "LayerFrame::paintEvent", "Unrecognized DropPlace");
            break;
        }
    }
}

LayerTreeWidget* LayerFrame::treeWidget()
{
    return static_cast<LayerTreeWidget*>(parent());
}

void LayerFrame::startDrag()
{
    LayerFrameDrag* drag = new LayerFrameDrag(this);
    drag->setLayerFrame(this);
    drag->exec();
}

void LayerFrame::dragEnterEvent(QDragEnterEvent *pe)
{
    if (pe->mimeData()->hasFormat(LayerFrameMime::mimeType()))
    {
        pe->acceptProposedAction();
    }
    QFrame::dragEnterEvent(pe);
}

void LayerFrame::dragMoveEvent(QDragMoveEvent *pe)
{
    if (pe->mimeData()->hasFormat(LayerFrameMime::mimeType()))
    {
        const LayerFrameMime* mimeData = static_cast<const LayerFrameMime*>(pe->mimeData());

        LayerFrame* toDrop = mimeData->layerFrame();

        if (toDrop->isSet())
        {
            QList<LayerFrame*> elements = toDrop->getElements();
            if (elements.contains(this))
            {
                return; //can't drop, because this is sub(+)layer
            }
        }

        QPoint pos = pe->pos();
        if (pos.y() >= 1 && pos.y() <= height()/2 )
        {
            setDropPlace(LayerFrame::TopLine);
            update();
            pe->acceptProposedAction();
        }
        else if (pos.y() <= height() - 1 && pos.y() >= height()/2)
        {
            setDropPlace(LayerFrame::BottomLine);
            update();
            pe->acceptProposedAction();
        }
        if (this->isSet())
        {
            if (pos.y() >= height()/6 && pos.y() <= height() - height()/6)
            {
                setDropPlace(LayerFrame::RectBoard);
                update();
                pe->acceptProposedAction();
            }
        }
    }
}

void LayerFrame::dragLeaveEvent(QDragLeaveEvent *pe)
{
    Q_UNUSED(pe);
    setDropPlace(LayerFrame::NoPlace);
    update();
}

void LayerFrame::dropEvent(QDropEvent *pe)
{
    const LayerFrameMime* mimeData = dynamic_cast<const LayerFrameMime*>(pe->mimeData());
    if (mimeData)
    {
        LayerFrame* toDrop = mimeData->layerFrame();
        LayerTreeWidget* tree = treeWidget();
        switch(_dropPlace)
        {
        case LayerFrame::TopLine:
            tree->insertLayerBefore(toDrop, this);
            break;
        case LayerFrame::BottomLine:
            tree->insertLayerAfter(toDrop, this);
            break;
        case LayerFrame::RectBoard:
            tree->insertLayerIntoSet(toDrop, this);
            break;
        default:
            break;
        }
    }

    setDropPlace(LayerFrame::NoPlace);
    update();
}

QList<LayerFrame*> LayerFrame::getElements()
{
    QList<LayerFrame*> elements;
    QStack<LayerFrame*> stk;

    stk.push(this);

    while(!stk.empty())
    {
        LayerFrame* current = stk.pop();
        if (current->isSet())
        {
            QList<LayerFrame*> list = current->subLayers();
            for (int i = list.size()-1; i>=0; i--)
            {
                stk.push(list[i]);
            }
        }
        elements.append(current);
    }

    return elements;
}

QString LayerFrame::layerName() const
{
    return layerNameLabel->text();
}

void LayerFrame::insertSubLayer(LayerFrame* subLayer, int ind)
{
    Q_ASSERT_X(this->isSet(), "LayerFrame::setSubLayer", "'this' object is not Layer Set");
    subLayer->_layerSet = this;
    int count = _layer->myLayerSetPointer()->count();
    Q_ASSERT_X(count == _subLayers.count(), "LayerFrame::setSubLayer", "Sets must be same size");
    _layer->myLayerSetPointer()->insert(count - ind, subLayer->layerEntity());
    _subLayers.insert(ind, subLayer);
}
void LayerFrame::removeSubLayer(LayerFrame* subLayer)
{
    Q_ASSERT_X(this->isSet(), "LayerFrame::removeSubLayer", "'this' object is not Layer Set");
    subLayer->_layerSet = nullptr;
    subLayer->setFixedWidth(upperWidth());
    _layer->myLayerSetPointer()->removeOne(subLayer->layerEntity());
    _subLayers.removeOne(subLayer);
}

void LayerFrame::contextMenuEvent(QContextMenuEvent *event)
{
    if (!isSet())
    {
        QMenu menu(this);
        QAction* fromFile = new QAction(tr("From file"), &menu);
        connect(fromFile, SIGNAL(triggered()), this, SLOT(chooseImageDialog()));
        menu.addAction(fromFile);
        menu.exec(event->globalPos());
    }

}

void LayerFrame::chooseImageDialog()
{
    QString fileName = QFileDialog::getOpenFileName(treeWidget(), tr("Choose Image"), QString(), tr("Image Files (*.png *.jpg)"));
    if (!fileName.isEmpty())
    {
        QImage image;
        if (!image.load(fileName))
        {
            qDebug("Can't load Image");
        }
        else
        {
            setLayerImage(image);
        }
    }
}

void LayerFrame::showLayer(bool show)
{
    _layer->setActive(show);
}
