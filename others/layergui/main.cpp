#include <QApplication>
#include "layerframe.h"
#include "layertreewidget.h"
#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QScrollArea>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget w;
    QPushButton* cmdLayer = new QPushButton("New Layer");
    QPushButton* cmdLayerSet = new QPushButton("New Layer Set");
    QPushButton* cmdDeleteLayer = new QPushButton("Delete current layer");
    QPushButton* cmdUpdateDebug = new QPushButton("Update info");
    QPushButton* cmdRender = new QPushButton("Render");
    LayerTreeWidget* ltw = new LayerTreeWidget(QSize(250, 250));
    QObject::connect(cmdLayer, SIGNAL(clicked()), ltw, SLOT(addNewLayer()));
    QObject::connect(cmdLayerSet, SIGNAL(clicked()), ltw, SLOT(addNewLayerSet()));
    QObject::connect(cmdDeleteLayer, SIGNAL(clicked()), ltw, SLOT(removeSelected()));
    QObject::connect(cmdUpdateDebug, SIGNAL(clicked()), ltw, SLOT(updateDebugInfo()));
    QObject::connect(cmdRender, SIGNAL(clicked()), ltw, SLOT(renderImage()));
    QTextEdit* textEdit = new QTextEdit();
    QObject::connect(ltw, SIGNAL(debugInfoUpdated(QString)), textEdit, SLOT(setText(QString)));
    QComboBox* cbox = new QComboBox;
    QStringList blendModes;
    blendModes << "Normal" << "Multiply" << "Darken" << "Screen" << "Overlay";
    cbox->addItems(blendModes);
    QObject::connect(cbox, SIGNAL(activated(QString)), ltw, SLOT(setBlendModeForSelected(QString)));
    QObject::connect(ltw, SIGNAL(blendModeChanged(QString)), cbox, SLOT(setEditText(QString)));

    QScrollArea* scrollArea = new QScrollArea;
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    scrollArea->setWidget(ltw);
    scrollArea->setWidgetResizable(true);
    scrollArea->setAlignment(Qt::AlignTop);

    QVBoxLayout* vbox = new QVBoxLayout();
    vbox->addWidget(cmdLayer);
    vbox->addWidget(cmdLayerSet);
    vbox->addWidget(cmdDeleteLayer);
    vbox->addWidget(cmdUpdateDebug);
    vbox->addWidget(cmdRender);
    vbox->addWidget(cbox);
    vbox->addWidget(scrollArea);
    vbox->addWidget(textEdit);

    QLabel* canvas = new QLabel();
    QObject::connect(ltw, SIGNAL(imageRendered(QPixmap)), canvas, SLOT(setPixmap(QPixmap)));
    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->addLayout(vbox);
    hbox->addWidget(canvas);

    w.setLayout(hbox);
    w.show();

    return a.exec();
}
