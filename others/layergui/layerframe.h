#ifndef LAYERFRAME_H
#define LAYERFRAME_H

#include <QFrame>
#include <QImage>
#include <QList>
#include <QMargins>
#include <QPalette>
#include <QPixmap>
#include <QPoint>
#include <QString>

class QCheckBox;
class QLabel;
class QVBoxLayout;

class LayerTreeWidget;
class AbstractLayer;

class MarkerEventFilter : public QObject
{
    Q_OBJECT

public:
    explicit MarkerEventFilter(QObject* parent = 0);
    bool eventFilter(QObject *obj, QEvent *e);
};

class LayerFrame : public QFrame
{
    Q_OBJECT
    
public:
    enum DropPlace {NoPlace, TopLine, BottomLine, RectBoard};

    explicit LayerFrame(bool isset, LayerTreeWidget *tree);
    ~LayerFrame();

    bool isSelected() const
    {
        return _selected;
    }
    bool isSet() const
    {
        return _isset;
    }

    LayerTreeWidget* treeWidget();

    //access to containing set
    LayerFrame* layerSet() const
    {
        return _layerSet;
    }
    bool isSubLayer() const
    {
        return _layerSet != nullptr;
    }
    bool isSubLayerOf(LayerFrame* set) const
    {
        Q_ASSERT_X(set && set->isSet(), "LayerFrame::isSubLayerOf", "set is not Layer Set");
        return layerSet() == set;
    }

    //access to sublayers if 'this' is Set
    QList<LayerFrame*> subLayers() const {
        return _subLayers;
    }
    LayerFrame* subLayerByIndex(int ind) const {
        return _subLayers.at(ind);
    }
    int subLayerCount() const {
        return _subLayers.size();
    }
    LayerFrame* lastSubLayer() const {
        return _subLayers.last();
    }
    int indexOfSubLayer(LayerFrame* layer) const {
        return _subLayers.indexOf(layer);
    }
    void insertSubLayer(LayerFrame* subLayer, int ind);
    void removeSubLayer(LayerFrame* subLayer);

    //maximum width for LayerFrame
    static constexpr int upperWidth() {
        return 152;
    }
    static constexpr int defaultHeight() {
        return 48;
    }

    //Return list, containing this LayerFrame and (if this LayerFrame is Set) all sublayers, subsublayers and etc.
    QList<LayerFrame*> getElements();

    QString layerName() const;

    AbstractLayer* layerEntity() {
        return _layer;
    }

public slots:
    void setThumbnail(QImage image);
    void setThumbnail(QPixmap pixmap);

    void setLayerImage(QImage image);

    void setLayerName(QString str);
    void setBlendMode(QString str);
    void setOpacity(int opacity);

    void select();
    void unselect();

    void chooseImageDialog();

    void showLayer(bool show);

signals:
    void selected();
    void unselected();

protected:
    void dropEvent(QDropEvent *pe);
    void dragEnterEvent(QDragEnterEvent *pe);
    void dragMoveEvent(QDragMoveEvent *pe);
    void dragLeaveEvent(QDragLeaveEvent *pe);
    void mouseMoveEvent(QMouseEvent *pe);
    void mousePressEvent(QMouseEvent *pe);
    void mouseReleaseEvent(QMouseEvent *pe);
    void paintEvent(QPaintEvent *pe);
    void contextMenuEvent(QContextMenuEvent *event);

private:
    void setDropPlace(DropPlace dropPlace)
    {
        _dropPlace = dropPlace;
    }
    void startDrag();

    QCheckBox* eyeBox;
    QCheckBox* markerBox;
    QLabel* thumbnailLabel;
    QLabel* layerNameLabel;
    QLabel* blendModeLabel;
    QLabel* opacityLabel;

    bool _selected;
    const bool _isset;
    LayerFrame* _layerSet;
    QList<LayerFrame*> _subLayers;
    DropPlace _dropPlace;
    QPoint _dragStartPos;

    bool _dragged;
    QPalette _defaultPalette;

    AbstractLayer* _layer;
};

#endif // LAYERFRAME_H
