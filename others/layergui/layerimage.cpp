//Own
#include "layerimage.h"

LayerImage::LayerImage(int w, int h) : QImage(w, h, QImage::Format_ARGB32)
{

}

LayerImage::LayerImage(const QImage &img) : QImage(img)
{

}

LayerImage::LayerImage(const LayerImage &li) : QImage(li)
{

}

bool LayerImage::hasColorAt(int x, int y) const
{
    return pixel(x, y) == 0u;
}

