#ifndef LAYER_H
#define LAYER_H

//Qt
#include <QImage>
#include <QColor>

//Own
#include "abstractlayer.h"
#include "layerimage.h"

class Layer : public AbstractLayer
{
public:
    Layer()
    {

    }

    Layer(QImage image)
    {
        setImage(image);
    }

    Layer(QImage image, Blending::BlendFunction blendMode)
    {
        setImage(image);
        setBlendMode(blendMode);
    }

    Layer* myLayerPointer() override
    {
        return this;
    }
    LayerSet* myLayerSetPointer() override
    {
        Q_ASSERT_X(false, "Layer::myLayerSetPointer", "Attempt to cast from Layer to LayerSet");
        throw;
    }
    bool isLayer() const override   { return true; }

    inline void setImage(QImage image) { img = image; }
    inline QImage image() const        { return img;  }

protected:
    QImage img;

};

#endif // LAYER_H
