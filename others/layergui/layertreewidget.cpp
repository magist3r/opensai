
//Own
#include "layerset.h"
#include "layertreewidget.h"
#include "layerframe.h"

//Qt
#include <QHash>
#include <QHBoxLayout>
#include <QVBoxLayout>

LayerTreeWidget::LayerTreeWidget(QSize laySize, QWidget *parent) :
    QWidget(parent), _selected(nullptr), _layerCounter(0), _setCounter(0), _layerSize(laySize)
{
    _upperLayerSet = new LayerSet();
    _vbox = new QVBoxLayout();
    addNewLayer();
    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->addLayout(_vbox);
    vbox->addStretch();
    setLayout(vbox);
}

void LayerTreeWidget::addNewLayer()
{
    LayerFrame* layerFrame = new LayerFrame(false, this);
    layerFrame->setLayerName(tr("Layer") + QString::number(_layerCounter));

    addLayerImpl(layerFrame);
    _layerCounter++;
}

void LayerTreeWidget::addNewLayerSet()
{
    LayerFrame* layerFrame = new LayerFrame(true, this);
    layerFrame->setLayerName(tr("Set") + QString::number(_setCounter));

    addLayerImpl(layerFrame);
    _setCounter++;
}

void LayerTreeWidget::addLayerImpl(LayerFrame *layerFrame)
{
    //insert into current Set
    if (_selected && _selected->isSet())
    {
        _selected->insertSubLayer(layerFrame, 0);
    }
    else if (_selected && _selected->isSubLayer())
    {
        _selected->layerSet()->insertSubLayer(layerFrame, 0);
    }
    //or insert as upper level layer
    else
    {
        insertUpperLayer(layerFrame, 0);
    }

    connect(layerFrame, SIGNAL(selected()), this, SLOT(wasSelected()));
    layerFrame->select();

    reconstructVBox();
}

void LayerTreeWidget::wasSelected()
{
    LayerFrame* layerFrame = qobject_cast<LayerFrame*>(sender());
    Q_ASSERT_X(layerFrame, "LayerTreeWidget::wasSelected", "sender must be LayerWidget");
    setSelected(layerFrame);
}

void LayerTreeWidget::setSelected(LayerFrame *layerFrame)
{
    if (_selected != nullptr)
    {
        _selected->unselect();
    }
    _selected = layerFrame;
    QString name = Blending::nameOfBlendMode(_selected->layerEntity()->blendMode());
    Q_ASSERT_X(!name.isEmpty(), "LayerTreeWidget::setSelected", "can't find name of blend mode");
    emit blendModeChanged(name);
}

QList<LayerFrame*> LayerTreeWidget::getAllLayerFrames()
{
    QList<LayerFrame*> toReturn;
    foreach(LayerFrame* layer, _upperLevelLayers)
    {
        toReturn.append(layer->getElements());
    }
    return toReturn;
}

//This is really UGLY! HACK HACK!
//We consider every item of _vbox is QHBoxLayout and delete it, then create again. This really sucks
void LayerTreeWidget::reconstructVBox()
{
    while (_vbox->count() != 0)
    {
        QHBoxLayout* hbox = static_cast<QHBoxLayout*>(_vbox->takeAt(0));
        delete hbox;
    }

    QHash<LayerFrame*, int> spaceHash;
    QHash<LayerFrame*, int>::iterator it;

    QList<LayerFrame*> layers = getAllLayerFrames();
    foreach (LayerFrame* layer, layers)
    {
        if (!layer->layerSet())
        {
            spaceHash.insert(layer, 0);
        }
        else
        {
            it = spaceHash.find(layer->layerSet());
            if (it == spaceHash.end())
            {
                Q_ASSERT_X(false, "LayerTreeWidget::reconstructVBox()", "invariant failed");
            }
            spaceHash.insert(layer, it.value()+LayerFrame::upperWidth()/10);
        }
    }

    foreach (LayerFrame* layer, layers)
    {
        QHBoxLayout* hbox = new QHBoxLayout;
        hbox->addSpacing(spaceHash[layer]);
        hbox->addWidget(layer);
        hbox->addStretch();

        _vbox->addLayout(hbox);
    }
}

void LayerTreeWidget::detachLayer(LayerFrame *layerFrame)
{
    if (layerFrame->isSubLayer())
    {
        layerFrame->layerSet()->removeSubLayer(layerFrame);
    }
    else
    {
        removeUpperLayer(layerFrame);
    }
}

void LayerTreeWidget::insertLayerAfter(LayerFrame *toInsert, LayerFrame *other)
{
    detachLayer(toInsert);
    if (other->isSubLayer())
    {
        int indInSet = other->layerSet()->subLayers().indexOf(other) + 1;
        other->layerSet()->insertSubLayer(toInsert, indInSet);
    }
    else
    {
        int ind = _upperLevelLayers.indexOf(other);
        insertUpperLayer(toInsert, ind+1);
    }

    reconstructVBox();
}

void LayerTreeWidget::insertLayerBefore(LayerFrame *toInsert, LayerFrame *other)
{
    detachLayer(toInsert);
    if (other->isSubLayer())
    {
        int indInSet = other->layerSet()->subLayers().indexOf(other);
        other->layerSet()->insertSubLayer(toInsert, indInSet);
    }
    else
    {
        int ind = _upperLevelLayers.indexOf(other);
        insertUpperLayer(toInsert, ind);
    }

    reconstructVBox();
}

void LayerTreeWidget::insertLayerIntoSet(LayerFrame *toInsert, LayerFrame *set)
{
    Q_ASSERT_X(set && set->isSet(), "LayerTreeWidget::insertLayerIntoSet", "set must be Set");
    detachLayer(toInsert);
    set->insertSubLayer(toInsert, 0);

    reconstructVBox();
}

void LayerTreeWidget::updateDebugInfo()
{
    QList<LayerFrame*> toDebug = getAllLayerFrames();

    QString debug = "Selected : " + _selected->layerName() + "\n";

    foreach(LayerFrame* layer, toDebug)
    {
        debug.append(layer->layerName());
        if (layer->isSubLayer())
        {
            debug.append(". SubLayer of " + layer->layerSet()->layerName());
        }
        else
        {
            debug.append(" . Upper Level Layer");
        }
        if (layer->isSet())
        {
            debug.append(". Sublayers: {");
            foreach(LayerFrame* layerFrame, layer->subLayers())
            {
                debug.append(layerFrame->layerName() + ' ');
            }
            debug.append('}');
        }
        debug.append('\n');

        emit debugInfoUpdated(debug);
    }
}

void LayerTreeWidget::removeSelected()
{
    LayerFrame* current = _selected;
    if (!current->isSubLayer()) //upper level layer
    {
        if (_upperLevelLayers.size() == 1)
            return; //can't remove if there is the only upper level in tree
        if (current == _upperLevelLayers.last())
        {
            removeUpperLayer(current);
            _upperLevelLayers.last()->select(); //select layer, that became last
        }
        else
        {
            _upperLevelLayers.at(_upperLevelLayers.indexOf(current)+1)->select(); //select next
            removeUpperLayer(current);
        }
    }
    else
    {
        LayerFrame* layerSet = current->layerSet();

        if (layerSet->subLayerCount() == 1)
        {
            layerSet->removeSubLayer(current);
            layerSet->select();
        }
        else if (current == layerSet->lastSubLayer())
        {
            layerSet->removeSubLayer(current);
            layerSet->lastSubLayer()->select();
        }
        else
        {
            int ind = layerSet->indexOfSubLayer(current);
            layerSet->removeSubLayer(current);
            layerSet->subLayerByIndex(ind)->select();
        }
    }

    foreach(LayerFrame* layerFrame, current->getElements())
    {
        layerFrame->deleteLater();
    }
    reconstructVBox();
}

void LayerTreeWidget::insertUpperLayer(LayerFrame *layerFrame, int ind)
{
    int count = _upperLayerSet->myLayerSetPointer()->count();
    Q_ASSERT_X(count == _upperLevelLayers.count(), "LayerFrame::setSubLayer", "Sets must be same size");
    _upperLayerSet->insert(count - ind, layerFrame->layerEntity());
    _upperLevelLayers.insert(ind, layerFrame);
}

void LayerTreeWidget::removeUpperLayer(LayerFrame *layerFrame)
{
    _upperLayerSet->removeOne(layerFrame->layerEntity());
    _upperLevelLayers.removeOne(layerFrame);
}

void LayerTreeWidget::renderImage()
{
    RenderTree renderTree = _upperLayerSet->compileRenderTree();
    QImage img = Rendering::renderImage(renderTree, _layerSize.width(), _layerSize.height());
    emit imageRendered(QPixmap::fromImage(img));
}

LayerTreeWidget::Memento LayerTreeWidget::toMemento()
{
    Memento memento;
    memento.layerSize = _layerSize;
    memento.upperLevelLayers = _upperLevelLayers;
    memento.selected = _selected;
    memento.layerCounter = _layerCounter;
    memento.setCounter = _setCounter;

    foreach (LayerFrame* layer, _upperLevelLayers) {
        layer->hide();
    }

    return memento;
}

void LayerTreeWidget::fromMemento(const Memento &memento)
{
    _layerSize = memento.layerSize;
    _upperLevelLayers = memento.upperLevelLayers;
    _selected = memento.selected;
    _layerCounter = memento.layerCounter;
    _setCounter = memento.setCounter;

    _upperLayerSet->clear();

    foreach (LayerFrame* layer, _upperLevelLayers) {
        layer->show();
        _upperLayerSet->append(layer->layerEntity());
    }
}

void LayerTreeWidget::setBlendModeForSelected(QString blendMode)
{
    _selected->setBlendMode(blendMode);
}
