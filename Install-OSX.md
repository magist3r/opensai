##Building OpenSAI on OS X 

This method is temporary. I will write a more automated configure script when time allows. Until then, the instructions below should give you a running copy of OpenSAI on OS X.

###Prerequisites
* Mac OS X 10.8 (10.7 and 10.6 may also work, it's untested)
* Homebrew (<http://mxcl.github.io/homebrew/>)
* Xcode (Mac App Store)
* Qt 4.8 or later
* GCC 4.7 or higher

###Building Dependencies
####A. Qt 4.8
1. Install Xcode from the Mac App Store
2. Install Homebrew from the URL above
3. In the Terminal, type `brew update`
4. In the Terminal, type `brew install qt`
5. Grab a sandwich or your beverage of preference while Qt compiles. Older machines can take a long time on this step.

####B. GCC 4.7
Due to an incomplete implementation of C++11 in the version of clang included with Xcode, we'll need to install GCC 4.7, an alternative compiler.

1. In the Terminal, type `brew tap homebrew/versions`
2. In the Terminal, type `brew install gcc47`
3. Grab a dessert or something else to drink. This will also take a while.

####C. QtPieMenu
QtPieMenu is used in OpenSAI to draw circular contextual menus.

1. In the Terminal, cd to the qtpiemenu library build directory (`/Core/UI/qtpiemenu_2.4_1/buildlib`)
2. Type `qmake buildlib.pro`
3. Type `make`

###Building OpenSAI
1. In the Terminal, cd to the OpenSAI source directory (`/Core/UI/`)
2. In the Terminal, type `qmake cai.pro -spec macx-g++`
3. Open `/Core/UI/Makefile` in your favorite text editor
4. Remove all occurances of `-Xarch_x86_64`
5. Change `gcc` to `gcc-4.7`
6. Change all occurances of `g++` to `g++-4.7`
7. Save and close the file
8. In the Terminal, type `make`. If everything has been set up correctly, OpenSAI will be compiled to `/Core/UI/OpenSAI.app`