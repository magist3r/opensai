#ifndef EXPANDER_H
#define EXPANDER_H

#include <QtGui>


class Expander : public QWidget {
    Q_OBJECT
public:
    explicit Expander(QWidget *parent = 0);

    virtual void addWidget(QWidget *widget);
    virtual void addLayout(QLayout *layout);
    void setName(QString name);

public slots:
    void rollThisBox();
protected:

private:
    bool unrolled;

    QVBoxLayout *layout;
    QPushButton *button;
    QWidget *widget;
    QLabel *text;
    QPixmap *idleImage;
    QPixmap *pushImage;

    QLabel *label;
};

#endif // EXPANDER_H
