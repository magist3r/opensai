
#include "basetools.h"

bool Pen::load(QString name)
{
    this->name = name;

    QSettings set(QDir::toNativeSeparators("./config/tools/" + name), QSettings::IniFormat);
    set.beginGroup("ToolParam");

    size     ->setValue( set.value("size",     100).toInt());
    minSize  ->setValue( set.value("minSize",  1).toInt());
    density  ->setValue( set.value("density",  100).toInt());
    blending ->setValue( set.value("blending", 100).toInt());
    edgeHardness = set.value("edgHard", 100).toInt();

    QString blndMode = set.value("blndMode", "normal").toString();
    if(blndMode.compare("normal") == 0)
        blender = Blending::normal;
    else if(blndMode.compare("multiply") == 0)
        blender = Blending::multiply;
    else if(blndMode.compare("screen") == 0)
        blender = Blending::screen;

    set.endGroup();
    return true;
}

bool Pen::save(QString name)
{
    QSettings set(QDir::toNativeSeparators("./cofig/tools/" + name), QSettings::IniFormat);
    return true;
}

void Pen::setupUi()
{
    layout = new QFormLayout();

    //size    = new QSlider();
    size    = new DisplaingSlider();
    size->setOrientation(Qt::Horizontal);
    minSize = new QSlider();
    minSize->setOrientation(Qt::Horizontal);
    density = new QSlider();
    density->setOrientation(Qt::Horizontal);

    layout->addRow("Size", size);
    layout->addRow("Min size", minSize);
    layout->addRow("Density", density);

    setLayout(layout);
}

void Pen::setSize(int value)
{
    this->size->setValue(value);
    emit sizeChanged(value);
}

void Pen::setMinSize(int value)
{
    this->minSize->setValue(value);
    emit minSizeChanged(value);
}

void Pen::setDensity(int value)
{
    this->density->setValue(value);
    emit densityChanged(value);
}



// This is kinda ugly :(
void Pen::action(QList<Dot> stroke)
{
    QPainter painter(layer);

    for(auto itr: stroke) {
        // getting the size of the signle dot iteration
        int size = this->size->value() - ( this->size->value() - this->minSize->value() ) * itr.pressure;


        // calculating the middle color
        // must be checked twice!
        qreal blendValue = this->blending->value() / this->blending->maximum();
        QRgb middle = mulColor(middleColor(*layer, this->shape, itr.pos, size), blendValue);
        QRgb color  = sumColors(middle, this->color.rgba() * (1.0 - blendValue));

        // 1% of opacity MULTIPLY TO count of %
        painter.setOpacity( ( 255 / 100.0 ) * this->density->value() * itr.pressure );

        // Actually painting
        for(int row_i = 0; row_i < size; ++row_i) {
            QRgb *line = (QRgb *)layer->scanLine( row_i + itr.pos.y() );
            for(int col_i = 0; col_i < size; ++col_i) {
                line += ( col_i + (unsigned int)itr.pos.x() );

                // middle * blendValue -- already precalculated above
                // middle * blendValue + ( blendMode(color, backDrop) * blendValue)
                layer->setPixel( col_i, row_i, Blending::applyForRgb( color, *line, blender ) );
            }
        }

     } // for-each stroke
}


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BINARY TOOL IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


void Binary::action(QList<Dot>)
{

}

void Binary::action(QList<Dot> stroke, QImage* image)
{
    QPainter painter(image);
    //QPen pen(color);
    //pen.setWidth(2);
    //painter.setPen(pen);
    //painter.setPen(color);
    for(auto itr : stroke) {
        int size = this->size->value();


        // getting the size of the signle dot iteration
        if(sizeByPressure->checkState())
            size = this->size->value() - (this->size->value() - this->minSize->value() )
                        * ( itr.pressure * ( this->hardness->value() / 0x64 ) );

        //int scale = size / this->size->maximum();
        //QRegion reg = QRegion( QBitmap::fromImage(shape.scaled(scale, scale)) ); // max is to be 100 here
        //painter.setClipRegion( reg );
        QRegion r1(QRect(itr.pos.x() - size / 0x2, itr.pos.y() - size / 0x2, size, size),    // r1: elliptic region
                       QRegion::Ellipse);
        painter.setClipRegion(r1);
        painter.setOpacity( opacity->value() );
        qDebug("Lalka");
        painter.fillRect(itr.pos.x() - size / 0x2, itr.pos.y() - size / 0x2, size, size, color);
        //painter.drawEllipse( itr.pos.x() - size / 0x2, itr.pos.y() - size / 0x2, size, size );

    }

}

bool Binary::load(QString name)
{
    name = name;
    return true;
}
bool Binary::save(QString name)
{
    name = name;
    return true;
}
void Binary::setupUi()
{
    layout = new QFormLayout();

    //size    = new QSlider();
    size    = new DisplaingSlider();
    size->setOrientation(Qt::Horizontal);
    size->setRange(1, 100);
    minSize = new QSlider();
    minSize->setOrientation(Qt::Horizontal);
    minSize->setRange(1, 100);
    density = new QSlider();
    density->setOrientation(Qt::Horizontal);
    density->setRange(1, 100);
    opacity = new QSlider();
    opacity->setOrientation(Qt::Horizontal);
    opacity->setRange(1, 100);
    hardness = new QSlider();
    hardness->setOrientation(Qt::Horizontal);
    hardness->setRange(1, 100);
    sizeByPressure = new QCheckBox();
    sizeByPressure->setChecked(true);

    shape = QImage(10, 10, QImage::Format_ARGB32);


    layout->addRow("Size", size);
    layout->addRow("Min size", minSize);
    layout->addRow("Density", density);
    layout->addRow("Opacity", opacity);
    layout->addRow("Hardness", hardness);
    layout->addRow("Size by press", sizeByPressure);

    setLayout(layout);
}


void Binary::setSize(int value)
{
    value = value;
}
void Binary::setMinSize(int value)
{
    value = value;
}
void Binary::setDensity(int value)
{
    value = value;
}
void Binary::setHardness(int value)
{
    value = value;
}
void Binary::setSizeByPress()
{

}


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ FLOOD FILL IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


void FloodFill::action(QList<Dot> stroke)
{
    /*
    this->floodFill(layer,
                    std::get<0>(stroke.at(0)).toPoint(),
              qRgb(0, 0, 0),
              color.rgb());
              */
    Q_UNUSED(stroke);
}

// this is ugly =(
void FloodFill::floodFill(LayerImage *target, QPoint point, QRgb oldColor, QRgb newColor)
{
    if(target->pixel(point) == oldColor) {
        target->setPixel(point, newColor);
        floodFill(target, point + QPoint(1, 0), oldColor, newColor);
        floodFill(target, point + QPoint(-1, 0), oldColor, newColor);
        floodFill(target, point + QPoint(0, 1), oldColor, newColor);
        floodFill(target, point + QPoint(0, -1), oldColor, newColor);
    }
}




bool FloodFill::load(QString name)
{
    name = name;
    return true;
}
bool FloodFill::save(QString name)
{
    name = name;
    return true;
}
void FloodFill::setupUi()
{}






