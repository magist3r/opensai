#ifndef DIPLAINGSLIDER_H
#define DIPLAINGSLIDER_H

// DisplaingSlider

#include <QAbstractSlider>
#include <QWidget>
#include <QStyle>
#include <QStylePainter>
#include <QMouseEvent>

class DisplaingSlider : public QAbstractSlider {
    Q_OBJECT
public:
    explicit DisplaingSlider(QWidget* parent = nullptr);
    ~DisplaingSlider();

protected:
    virtual void paintEvent(QPaintEvent *e);
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseMoveEvent(QMouseEvent*);


    QPoint pos[2];
};


#endif // DIPLAINGSLIDER_H
