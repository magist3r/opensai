
#include "mathematic.h"
#include <cstring>

namespace math {

template< template <typename> class T, typename C  >
typename BezierCurve<T, C>::point BezierCurve<T, C>::getBezierCurvePoint(BezierCurve<T, C>::path p, double t)
{
		C r;
		for(size_t i = 0; i < p.size(); ++i) {
			r += p[i] * bPolynom(i, p.size(), t);
		}
		return r;
}

template< template <typename> class T, typename C  >
typename BezierCurve<T, C>::path BezierCurve<T, C>::getBezierCurve(BezierCurve<T, C>::path p)
{
	path result;
	size_t size = p.size();
	for(size_t i = 0; i < size; ++i) {
		double t = i / (double)size;
		result.push_back(getBezierCurvePoint(p, t));
	}
	return result;
}

template< template <typename> class T, typename C  >
long long BezierCurve<T, C>::fact(long value)
{
	if(value != 0)
		return fact(value - 1) * value;
	else
		return 1;
}

template< template <typename> class T, typename C  >
long long BezierCurve<T, C>::binom(int i, int n)
{
	return fact(n) / ( fact(i) * fact(n - i) );
}

template< template <typename> class T, typename C  >
double BezierCurve<T, C>::bPolynom(int i, int n, double t)
{
	return binom(i, n) * pow(t, i) * pow(1.0f - t, n - i);
}

template<typename T> class StdVector: public std::vector<T> {};




} // namespace math
