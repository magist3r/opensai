//Own
#include "blendmode.h"

QRgb Blending::applyForRgb(const QRgb source, const QRgb backdrop, const BlendFunction blend)
{
    const int as = qAlpha(source);
    const int ab = qAlpha(backdrop);

    const int alpha = blendAlpha(as, ab);

    if (alpha)
    {
        const int red   = composite(qRed(source),   qRed(backdrop),   as, ab, blend) / alpha;
        const int green = composite(qGreen(source), qGreen(backdrop), as, ab, blend) / alpha;
        const int blue  = composite(qBlue(source),  qBlue(backdrop),  as, ab, blend) / alpha;

        return qRgba(red, green, blue, alpha);
    }
    else return 0u; //transparent
}

int Blending::multiply(int cb, int cs)
{
    return cb*cs/0xFF;
}

int Blending::normal(int cb, int cs)
{
    Q_UNUSED(cb);
    return cs;
}

int Blending::darken(int cb, int cs)
{
    return (cb > cs)? cs : cb;
}

int Blending::screen(int cb, int cs)
{
    return (cb + cs - (cb * cs))/0xFF;
}

int Blending::overlay(int cb, int cs)
{
    if(cs <= 127)
        return multiply(cb, 2 * cs);
    else
        return screen(cb, 2 * cs - 0xFF);
}

Blending::BlendMapper* Blending::blendMapper()
{
    static Blending::BlendMapper bm;
    return &bm;
}

Blending::BlendMapper::BlendMapper()
{
    QString names[] = {"Normal", "Multiply", "Darken", "Screen", "Overlay"};
    Blending::BlendFunction modes[] = { Blending::normal,
                                        Blending::multiply,
                                        Blending::darken,
                                        Blending::screen,
                                        Blending::overlay
                                      };

    for (quint32 i=0; i<sizeof(names)/sizeof(names[0]); ++i)
    {
        blendMap.insert(names[i], modes[i]);
    }
}

Blending::BlendFunction Blending::BlendMapper::blendModeFromName(QString name) const
{
    QMap<QString, BlendFunction>::const_iterator it = blendMap.find(name);
    if (it == blendMap.end())
    {
        qDebug("No such blend function");
        return nullptr;
    }
    return it.value();
}

QString Blending::BlendMapper::nameOfBlendMode(BlendFunction blendMode) const
{
    QString str = blendMap.key(blendMode);
    if (str.isEmpty())
    {
        qDebug("No name for blend mode function");
    }
    return str;
}
