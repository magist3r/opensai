#include "colorslider.h"
#include <QBrush>
#include <QPen>
#include <QLinearGradient>
#include <QPainter>

ColorSlider::ColorSlider(char colorSymbol, QWidget *parent) :
    QSlider(parent), c(colorSymbol)
{
    setOrientation(Qt::Horizontal);
}

void ColorSlider::paintEvent(QPaintEvent *ev)
{
    QLinearGradient gradient(0,0, width()-1, height()-1);

    int accuracy = 15;
    for (int i=0; i<=accuracy; ++i)
    {
        qreal pos = static_cast<qreal>(i)/static_cast<qreal>(accuracy);
        QColor color;
        if (c == 'r')
        {
            color = QColor::fromRgbF(pos, commonColor.greenF(), commonColor.blueF());
        }
        else if (c == 'g')
        {
            color = QColor::fromRgbF(commonColor.redF(), pos, commonColor.blueF());
        }
        else // (c == 'b')
        {
            color = QColor::fromRgbF(commonColor.redF(), commonColor.greenF(), pos);
        }
        gradient.setColorAt(pos, color);
    }

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(Qt::NoBrush);
    painter.setPen(QPen(QBrush(gradient), height()));
    painter.drawRect(0,0, width(), height());

    QSlider::paintEvent(ev);
}

void ColorSlider::setCommonColor(QColor color)
{
    commonColor = color;
    update();
}
