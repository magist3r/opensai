#ifndef RENDERTREE_H
#define RENDERTREE_H

//Qt
#include <QColor>
#include <QImage>

//Own
#include "abstractlayer.h"

struct RenderNode;

typedef QList<RenderNode> RenderTree;

struct RenderNode
{
    RenderNode(const RenderNode& other) : layer(other.layer), branchSet(other.branchSet)
    {
        //pass
    }

    RenderNode() : layer(nullptr) //invalid RenderNode
    {
        //pass
    }

    RenderNode(AbstractLayer* lay)
        : layer(lay)
    {
        //pass
    }

    RenderNode(AbstractLayer* layerSet, RenderTree branch)
        : layer(layerSet), branchSet(branch)
    {
        //pass
    }

    AbstractLayer* layer;
    RenderTree branchSet;
};

//see rendertree.cpp
namespace Rendering
{
QRgb renderPixel(const RenderTree& renderTree, int x, int y);

QImage renderImage(const RenderTree& renderTree, const int w, const int h);
}

#endif // RENDERTREE_H
