#ifndef CANVAS_H
#define CANVAS_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPoint>
#include <QImage>
#include <QPixmap>
#include <QWheelEvent>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QShortcut>
#include <QScrollBar>

#include <cmath>

#include "logger.h"
#include "tool.h"

#include "qtpiemenu-2.4_1/src/qtpiemenu.h"

class LayerImage;

constexpr double PI = 3.14159265;


class Canvas : public QGraphicsView {
    Q_OBJECT
public:
    explicit Canvas(QSize size, QWidget* parent = nullptr);
    ~Canvas();

signals:

public slots:
    void repaint(QImage value);
    void repaint(QPixmap value);

    void rotateLeft()
    { rotate(10.0);  }
    void rotateRight()
    { rotate(-10.0); }

    void moveLeft()
    {
        move(25, 0);
    }

    void setDefaults()
    {
        //resetMatrix();
        resetTransform();
        centerOn(item);
    }

    void rotorEvent();
    void panEvent();


protected:
    virtual void wheelEvent       (QWheelEvent* e) override;
    virtual void mousePressEvent  (QMouseEvent* e) override;
    virtual void mouseMoveEvent   (QMouseEvent* e) override;
    virtual void mouseReleaseEvent(QMouseEvent* e) override;

private:

    void canvasEvent();

    enum class  MODES {PAINT = 0, PAN = 1, ROTOR = 2};

    QGraphicsScene scene;
    QPixmap        canvas;
    QPoint         dots[2];
    MODES          mode = MODES::PAINT;


    QGraphicsPixmapItem* item;

    double zoom;
    double angle;

};

#endif // CANVAS_H
