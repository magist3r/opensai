/*
 *   Copyright 2013 Derptech
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * */

#include "logger.h"


//Logger
Logger::Logger()
    : numWarnings (0U), numErrors (0U)
{

}

void Logger::addDevice(LoggerDevice* device)
{
    device->setParent(this);
    _devices.append(device);
}
QList<LoggerDevice*> Logger::devices() const
{
    return _devices;
}
void Logger::removeDevice(LoggerDevice* device)
{
    _devices.removeOne(device);
}

Logger::~Logger()
{

}

Logger &operator << (Logger &logger, Logger::LogState state)
{
    foreach(LoggerDevice* device, logger.devices())
    {
        device->setState(state);
    }
    return logger;
}

Logger &operator << (Logger &logger, QString str)
{
    foreach(LoggerDevice* device, logger.devices())
    {
        device->write(str);
    }
    return logger;
}

//LogWindow

LogWindow::LogWindow()
{
    window = new QWidget();
    window->setAttribute(Qt::WA_DeleteOnClose);
    window->setVisible(true);
    window->setWindowTitle(QObject::tr("Error log"));
    window->setMinimumSize(300, 120);
    window->setMaximumSize(450, 250);

    table = new QTableWidget();
    table->setColumnCount(2);
    table->setRowCount(0);
    table->setColumnWidth(0, 45);
    table->setColumnWidth(1, 300);
    table->setColumnWidth(2, 50);
    table->insertRow(0);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(table);
    window->setLayout(layout);
}

void LogWindow::setState(Logger::LogState state)
{
    QTableWidgetItem* item;
    switch(state)
    {
    case Logger::Info:
        item = new QTableWidgetItem();
        item->setText("Info");
        table->setItem(table->rowCount()-1, 0, item);
        break;
    case Logger::Warning:
        item = new QTableWidgetItem();
        item->setText("Warning");
        table->setItem(table->rowCount()-1, 0, item);
        break;
    case Logger::Error:
        item = new QTableWidgetItem();
        item->setText("Error");
        table->setItem(table->rowCount()-1, 0, item);
        break;
    case Logger::End:
        table->insertRow(table->rowCount());
        break;
    }
}

void LogWindow::write(QString str)
{
    QTableWidgetItem* item = table->item(table->rowCount()-1, 1);
    if (item)
    {
        item->setText(item->text() + str);
    }
    else
    {
        item = new QTableWidgetItem(str);
        table->setItem(table->rowCount()-1, 1, item);
    }
}

void LogWindow::show()
{
    window->show();
}

void LogWindow::hide()
{
    window->hide();
}

//LogFile

LogFile::LogFile(QString fileName, QObject* parent) : LoggerDevice(parent), file(fileName)
{
    if (file.open(QIODevice::Text | QIODevice::WriteOnly))
    {
        stream.setDevice(&file);
    }
}
LogFile::LogFile(FILE* fp, QObject* parent) : LoggerDevice(parent)
{
    if (file.open(fp, QIODevice::Text | QIODevice::WriteOnly))
    {
        stream.setDevice(&file);
    }
}

void LogFile::setState(Logger::LogState state)
{
    switch(state)
    {
    case Logger::Info:
        stream << tr("[Info] ");
        break;
    case Logger::Warning:
        stream << tr("[Warning] ");
        break;
    case Logger::Error:
        stream << tr("[Error] ");
        break;
    case Logger::End:
        stream << "\n";
        break;
    }
    stream.flush();
}

void LogFile::write(QString str)
{
    stream << str;
}

bool LogFile::isValid() const
{
    return file.isOpen() && file.isTextModeEnabled() && file.isWritable();
}

QString LogFile::lastError() const
{
    return file.errorString();
}



