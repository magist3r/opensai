#include "layerframemime.h"

LayerFrameMime::LayerFrameMime()
{
}

void LayerFrameMime::setLayerFrame(LayerFrame* layerFrame)
{
    _layerFrame = layerFrame;
    setData(mimeType(), QByteArray());
}



void LayerFrameDrag::setLayerFrame(LayerFrame* layerFrame)
{
    LayerFrameMime* mimeData = new LayerFrameMime();
    mimeData->setLayerFrame(layerFrame);
    setMimeData(mimeData);
}

LayerFrame* LayerFrameMime::layerFrame() const
{
    return _layerFrame;
}
