#include "displaingslider.h"


DisplaingSlider::DisplaingSlider(QWidget *parent)
{
    setParent(parent);
}

DisplaingSlider::~DisplaingSlider() {}


void DisplaingSlider::paintEvent(QPaintEvent*)
{
    QStylePainter painter(this);

    int x = (pos[1].x() >= 0)? (pos[1].x() <= width())? pos[1].x(): width()+1: 0;

    qreal per = width() / 100.0f;
    int val = (maximum() - minimum()) / 100.0f * (x / per);
    setValue(val);

    QFont f = painter.font();
    f.setPixelSize(height() * 0.9f);
    painter.setFont(f);

    painter.fillRect(0, 0, x-2, height()-1, palette().dark());

    QRect rect;
    rect.setX (width () - height() * 1.9f);
    rect.setY((height() - height() * 0.9f)/2);

    rect.setWidth (height() * 2);
    rect.setHeight(height() * 0.9f);
    painter.drawText(rect, Qt::AlignCenter, QString::number(val));
}

void DisplaingSlider::mousePressEvent(QMouseEvent *e)
{
    pos[0] = pos[1] = e->pos();
    update();
}

void DisplaingSlider::mouseMoveEvent(QMouseEvent *e)
{
    pos[0] = pos[1];
    pos[1] = e->pos();
    update();
}



