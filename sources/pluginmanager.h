#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QDir>
#include <QVector>
#include <QPluginLoader>
#include <QLibrary>
#include <QStringList>
#include <QVector>

#include "path.h"

typedef QPluginLoader Plugin;

template<typename T>
class PluginManager {

    typedef T* PluginType;
public:
     PluginManager();
    ~PluginManager();

    bool                 loadAllPlugins(QString dirName);
    QVector<PluginType>& getPlugins();

private:
    PluginType loadPlugin(QString name);

    QVector<PluginType> plugins;
};


#endif // PLUGINMANAGER_H
