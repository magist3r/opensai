#ifndef BLENDMODE_H
#define BLENDMODE_H

//Qt
#include <QColor>
#include <QMap>
#include <QString>

namespace Blending
{

typedef int(*BlendFunction)(int /*backdrop*/, int /*source*/);

inline int blendAlpha(const int as, const int ab)
{
    return as + (ab*(0xFF - as))/0xFF;
}

inline int composite(const int cs, const int cb, const int as, int ab, const BlendFunction blend)
{
    return ((as*((0xFF-ab)*cs + ab * blend(cb, cs)) + (0xFF - as)*ab*cb))/0xFF;
}

QRgb applyForRgb(const QRgb source, const QRgb backdrop, const BlendFunction blend);

BlendFunction blendModeFromName(QString name);
QString nameOfBlendMode(BlendFunction blendMode);
//Blend modes

int normal(int cb, int cs);
int multiply(int cb, int cs);
int darken(int cb, int cs);
int screen(int cb, int cs);
int overlay(int cb, int cs);

class BlendMapper
{
public:
    BlendMapper();

    BlendFunction blendModeFromName(QString name) const;
    QString nameOfBlendMode(BlendFunction blendMode) const;

    QList<QString> blendModeNames() const
    {
        return blendMap.keys();
    }

private:
    QMap<QString /*blendModeName*/, BlendFunction /*blendModeFunc*/> blendMap;
};


BlendMapper* blendMapper();

}




#endif // BLENDMODE_H
