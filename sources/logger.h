/*
 *   Copyright 2013 Derptech
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * */

#ifndef LOGGER_H
#define LOGGER_H

#include <QString>
#include <QWidget>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QList>

#include <QFile>
#include <QTextStream>

using namespace std;

#define logIt Logger::globalInstance()

class LoggerDevice;

/**
 * @class The Logger class
 * @brief the class that logging, nothing more
 */
class Logger : public QObject
{
public:
    enum LogState { Info = 0, Warning = 1, Error = 2, End = 3 };

    ~Logger ();

    void addDevice(LoggerDevice* device);
    QList<LoggerDevice*> devices() const;
    void removeDevice(LoggerDevice* device);

    static Logger &globalInstance()
    {
        static Logger instance;
        return instance;
    }

    Logger(const Logger &) = delete;
    Logger &operator= (const Logger &) = delete;

private:

    explicit Logger();

    unsigned int  numWarnings;
    unsigned int  numErrors;

    LogState _lastState;

    QList<LoggerDevice*> _devices;
};


Logger &operator << (Logger &logger, Logger::LogState state);
Logger &operator << (Logger &logger, QString str);

class LoggerDevice : public QObject
{
public:
    LoggerDevice(QObject* parent = 0) : QObject(parent) {}
    virtual void setState(Logger::LogState state) = 0;
    virtual void write(QString str) = 0;
    virtual bool isValid() const {
        return false;
    }
    virtual QString lastError() const {
        return "";
    }
};

class LogFile : public LoggerDevice
{
public:
    LogFile(QString fileName, QObject* parent = 0);
    LogFile(FILE* fp, QObject* parent = 0);

    void setState(Logger::LogState state) override;
    void write(QString str) override;
    bool isValid() const override;
    QString lastError() const override;

private:

    QFile file;
    QTextStream stream;
};

class LogWindow : public LoggerDevice
{
public:
    LogWindow();
    void setState(Logger::LogState state) override;
    void write(QString str) override;
    bool isValid() const override{
        return true;
    }
    QWidget* widget() const {
        return window;
    }

public slots:
    void show();
    void hide();

private:
    QWidget      *window;
    QTableWidget *table;
};

#endif // LOGGER_H








