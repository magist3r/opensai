#ifndef COLORSLIDER_H
#define COLORSLIDER_H

#include <QColor>
#include <QSlider>

class ColorSlider : public QSlider
{
    Q_OBJECT
public:
    explicit ColorSlider(char colorSymbol, QWidget *parent = 0);
    
signals:
    
public slots:
    void setCommonColor(QColor color);

protected:
    void paintEvent(QPaintEvent *ev);

    QColor commonColor;
    char c;
};

#endif // COLORSLIDER_H
