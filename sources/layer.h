#ifndef LAYER_H
#define LAYER_H

//Qt
#include <QImage>
#include <QColor>

//Own
#include "abstractlayer.h"
#include "layerimage.h"

class Layer : public AbstractLayer
{
public:
    Layer();
    Layer(QImage image);
    Layer(QImage image, Blending::BlendFunction blendMode);
    ~Layer();

    Layer* myLayerPointer() override;
    LayerSet* myLayerSetPointer() override;
    bool isLayer() const override;

    void setImage(QImage image);
    LayerImage* image() const;

protected:
    LayerImage* img;

};

#endif // LAYER_H
