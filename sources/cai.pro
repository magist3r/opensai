#-------------------------------------------------
#
# Project created by QtCreator 2013-03-12T00:18:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = cai
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

RC_FILE = cai.rc

SOURCES += main.cpp \
    colorwheel.cpp \
    pieceslist.cpp \
    expander.cpp \
    mainwindow.cpp \
    logger.cpp \
    canvas.cpp \
    tool.cpp \
    newwindow.cpp \
    rendertree.cpp \
    layerset.cpp \
    layerimage.cpp \
    blendmode.cpp \
    abstractlayer.cpp \
    basetools.cpp \
    adjuvantwidgets.cpp \
    colorslider.cpp \
    rgbwidget.cpp \
    colorpair.cpp \
    layertreewidget.cpp \
    layerframemime.cpp \
    layerframe.cpp \
    mathematic.cpp \
    layer.cpp \
    configwindow.cpp \
    config.cpp \
    path.cpp \
    pluginmanager.cpp \
    displaingslider.cpp

HEADERS  += \
    canvas.h \
    mainwindow.h \
    colorwheel.h \
    pieceslist.h \
    colorpair.h \
    preview.h \
    config.h \
    expander.h \
    logger.h \
    tool.h \
    newwindow.h \
    rendertree.h \
    layerset.h \
    layerimage.h \
    layer.h \
    blendmode.h \
    abstractlayer.h \
    basetools.h \
    adjuvantwidgets.h \
    colorslider.h \
    rgbwidget.h \
    layertreewidget.h \
    layerframemime.h \
    layerframe.h \
    mathematic.h \
    configwindow.h \
    path.h \
    pluginmanager.h \
    displaingslider.h

FORMS    +=

RESOURCES += \
    resources.qrc

macx {
    TARGET = OpenSAI
    ICON = icon.icns
	} else {
	target.path = /${DESTDIR}/usr/bin
	target.files = cai
}


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/qtpiemenu-2.4_1/lib/ -llibQtSolutions_PieMenu-2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/qtpiemenu-2.4_1/lib/ -llibQtSolutions_PieMenu-2d
else:unix:!macx: LIBS += -L$$PWD/qtpiemenu-2.4_1/lib/ -lQtSolutions_PieMenu-2.4
else:macx: LIBS += -L$$PWD/qtpiemenu-2.4_1/lib/ -lQtSolutions_PieMenu-2.4

INCLUDEPATH += $$PWD/qtpiemenu-2.4_1/src
DEPENDPATH += $$PWD/qtpiemenu-2.4_1/src
