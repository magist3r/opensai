
#include <QTextStream>
#include <QFile>

#include <QMessageBox>

#include "config.h"


Settings::Settings()
{

}

Settings::~Settings()
{
    //saveConfig();
}



void Settings::setFileType(QString value)
{
    FILE_TYPE = value;
}

// reads the data formatted like following:
// name = value ; config
Config Settings::loadFile(QString name)
{
    QFile data(name);
    data.open(QFile::ReadOnly);

    if(data.error()>0) {
        return {};
    }

    Config localConfig;

    QTextStream stream(&data);
    QString str;
    while(!stream.atEnd()) {
       str = stream.readLine();

       QStringList list = str.split('=', QString::KeepEmptyParts);
       QStringList value = list[1].split(";", QString::SkipEmptyParts);
       localConfig[list[0].trimmed()] = value[0].replace(" ", "");
    }
    data.close();
    return localConfig;
}

void Settings::saveFile(QString name, Config value)
{
    QFile file(name);
    file.open(QFile::WriteOnly);

    QTextStream stream(&file);
    for(QVariant itr : value) {
        stream << value.key(itr) << " = " << itr.toString() /* comment here... =/ */ << endl;
    }

}

bool Settings::loadConfig(QString path)
{
    Path localPath(path);
    QDir localDir(localPath.getPath());
    QStringList files = localDir.entryList({"*." + FILE_TYPE }, QDir::Files);

    for(QString& filesItr : files) {
        data[filesItr.replace("." + FILE_TYPE, "")] = loadFile(localPath.addDir(filesItr));
        localPath.moveUp();
    }
    return true;
}


bool Settings::saveConfig(QString path)
{
    Path localPath(path);
    for(Config itr : data) {
        saveFile(localPath.addDir(data.key(itr) + "." + FILE_TYPE), itr);
    }

    return true;
}















