
#include "expander.h"

Expander::Expander(QWidget *parent) : QWidget(parent)
    {

        layout = new QVBoxLayout;
        widget = new QWidget;
        widget->setMinimumSize(150, 100);
        widget->setLayout(layout);
        layout->setMargin(0);
        layout->setSpacing(1);
        layout->setAlignment(Qt::AlignTop);
        // control
        QHBoxLayout *controlLayout = new QHBoxLayout;
        controlLayout->setMargin(0);
        controlLayout->setSpacing(4);
        controlLayout->setAlignment(Qt::AlignLeft);

        QLabel *label = new QLabel("Advanced");
        button  = new QPushButton;
        controlLayout->addWidget(button);
        controlLayout->addWidget(label);

        idleImage = new QPixmap(":res/plus_idle.png");
        pushImage = new QPixmap(":res/plus_pushed.png");

        button->setMaximumSize(15, 15);
        button->setMinimumSize(15, 15);
        button->setIcon(QIcon(*idleImage));
        button->setIconSize(QSize(15, 15));

        connect(button, SIGNAL(clicked()), this, SLOT(rollThisBox()));
        unrolled = false;
        // base
        QVBoxLayout *base = new QVBoxLayout;
        base->setAlignment(Qt::AlignTop);

        base->addLayout(controlLayout);
        base->addWidget(widget);
        this->setLayout(base);
    }

    void Expander::addWidget(QWidget *widget) {
        this->layout->addWidget(widget);
    }

    void Expander::addLayout(QLayout *layout) {
        this->layout->addLayout(layout);
    }


    void Expander::rollThisBox()
    {
        if(unrolled) {
            this->widget->hide();
            button->setIcon(QIcon(*idleImage));
            unrolled = false;
        } else {
            button->setIcon(QIcon(*pushImage));
            this->widget->show();
            unrolled = true;
        }
    }

    void Expander::setName(QString name)
    {
        this->label->setText(name);
    }
