#ifndef ADJUVANTWIDGETS_H
#define ADJUVANTWIDGETS_H

#include <QImage>
#include <QString>
#include <QRadioButton>
#include <QList>

#include <QAbstractSlider>

#include <tuple>
#include <cmath>

#include "expander.h"
#include "basetools.h"

// shitty code...
class Frame : public QFrame {
    Q_OBJECT
public:
    explicit Frame(QFrame* parent  = nullptr)
        :QFrame(parent)
    {
        //setFocusPolicy(Qt::WheelFocus);
        //setFocus(Qt::MouseFocusReason);
        setMouseTracking(true);
    }
    ~Frame() {}

protected:
    virtual void mouseMoveEvent(QMouseEvent *e)
    {
        if(rect().contains(e->pos()))
            /*
        if(e->pos().x() >= 0 &&
           e->pos().x() <= width() &&
           e->pos().y() >= 0 &&
           e->pos().y() <= height())*/
            setFocus();
        else
            clearFocus();
    }
};


class ImageRadioGroup : public QWidget {
    Q_OBJECT
public:
    explicit ImageRadioGroup(QWidget *parent = 0);
    ~ImageRadioGroup() {}

    void addButton(QIcon icon);
    void setupUi();

signals:
    void buttonSelected(int value);

public slots:
    void selectButton(int value) { Q_UNUSED(value); }

protected:

    QList<QPushButton*>  *buttons;
    QHBoxLayout          *layout;

};


class ImageCheckGroup : public QWidget {

};

/**
 * @class Widget for navigation on canvas and preview
 * @brief The NavigationWidget class
 */
class NavigationWidget : public QWidget {
    Q_OBJECT
public:
    explicit NavigationWidget(QWidget *parent = 0);
    ~NavigationWidget();

signals:

    void moved(QPointF value);
    void rotated(qreal value);
    void scaled(qreal value);

    void moved(QPoint value);
    void rotated(int value);
    void scaled(int value);

public slots:

    void setImage(QPixmap value);

    void rotateUp(qreal value);
    void scaleUp(qreal value);
    void moveUp(QPointF value);

protected:

    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);

private:
    QPixmap  buffer;
    QRect    navi;
    QPointF  bufferPos;
    QSize    viewPortSize;
    qreal    angle;
    qreal    scale;

    QPointF   points[2];
    bool mousePressed;
};




class ToolsItem : public QFrame {
    Q_OBJECT
public:
    explicit ToolsItem(QFrame *parent  = 0);

    ToolsItem(ToolBase *tool, QFrame *parent  = 0);
    virtual ~ToolsItem();

    void      setTool(ToolBase *value);
    ToolBase* getTool();

signals:
    void clicked(ToolBase *value);

public slots:

protected:
    void mousePressEvent(QMouseEvent *);


    ToolBase    *tool;
    QLabel       image;
    QLabel       name;
    QVBoxLayout  layout;
};


class ToolsGridWidget : public QWidget {
    Q_OBJECT
public:

    explicit ToolsGridWidget(QWidget *parent = 0);
    ~ToolsGridWidget();

    void addTool(int row, int col, ToolBase *value);
    void addTool(ToolBase *value);

signals:
    void selectedTool(ToolBase *tool);

public slots:

protected:
    virtual void mousePressEvent(QMouseEvent *);
    virtual void paintEvent(QPaintEvent *event);
    virtual void resizeEvent(QResizeEvent *);

private:

    QVector<ToolsItem*>   tools;
    QMenu                *contextMenu;
    QGridLayout           layout;
    QPixmap              *selec;
    qint32                columns;
    qint32                rows;
};

class SpSlider : public QSlider {
    Q_OBJECT
public:
    explicit SpSlider(QWidget* parent = nullptr)
        :QSlider(parent)
    {}
    ~SpSlider() {}

protected:

private:

};

#endif // ADJUVANTWODGETS_H

































