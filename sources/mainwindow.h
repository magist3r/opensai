#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>
#include <QMenuBar>
#include <QMap>
#include <QSpacerItem>
#include <QSplitter>

#include <QtGui>

#include "canvas.h"
#include "colorwheel.h"
#include "pieceslist.h"
#include "colorpair.h"
#include "preview.h"
#include "config.h"
#include "expander.h"
#include "tool.h"
#include "newwindow.h"
#include "logger.h"
#include "adjuvantwidgets.h"
#include "rgbwidget.h"
#include "layertreewidget.h"
#include "configwindow.h"
#include "config.h"



class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();


public slots:

    void createNewCanvas(QSize size, QString name);
    void hidePanels();
    void setFullScreen();

    void setLayersEnabled(bool b);
    void subWindowDestroyedSlot(QMdiSubWindow* mdiWindow);
    void subWindowActivatedSlot(QMdiSubWindow* mdiWindow);
    void setImageForCurrentWindow(QImage image);

    void applySettings();

signals:
    void newCanvasCreated(QSize size, QString name);
    void canvasUpdate(QPixmap value);


private slots:
    void rotorUp();
    void rotorDown();
    void rotorByList(int index);
    void rotorManual(int angle);
    void scaleManual(int scale);

    void moveManual(QPointF move);
    void selectTool(ToolBase *value);
    void doAction(QList<Dot> stroke);


protected:

    virtual void  keyPressEvent(QKeyEvent *event);

private:

    void setupMenu();
    void setupUi();

    void saveLastTreeToMemento();

    int min_width;
    int min_heigth;

    ToolsGridWidget         *toolGrid;
    ToolBase                *selectedTool;
    ColorPair               *colorPair;

    QSplitter               *splitterRight;
    QSplitter               *splitterLeft;

    Frame*                   wAreaFrame;
    QMdiArea                *workArea;
    QWidget                 *center;
    QMenuBar                *menu;
    QHBoxLayout             *layoutHorizontal;
    QVBoxLayout             *layoutVertical;
    QStatusBar              *statusBar;


    QLabel                  *zoomLabel;
    QSlider                 *zoomSlider;
    QPushButton             *zoomOutCmd;
    QPushButton             *zoomInCmd;
    QPushButton             *zoomResetCmd;

    QLabel                  *angleLabel;
    QSlider                 *angleSlider;
    QPushButton             *angleOutCmd;
    QPushButton             *angleInCmd;
    QPushButton             *angleResetCmd;

    QComboBox               *blendComboBox;
    QSpinBox                *opacityBox;
    QScrollArea             *layerScrollArea;
    LayerTreeWidget         *layerTree;
    QPushButton             *newLayerCmd;
    QPushButton             *newLayerSetCmd;
    QPushButton             *deleteLayerCmd;

    QPushButton             *showWheelCmd;
    QPushButton             *showRgbCmd;
    ColorWheel              *colorWheel;
    RgbWidget               *rgbWidget;

    QWidget                 *panelRight;
    QVBoxLayout             *rigthLayout;
    QWidget                 *panelLeft;
    QVBoxLayout             *leftLayout;
    //PreviewWidget           *preView;

    // tools bar...
    QWidget                 *toolBar;
    QHBoxLayout             *toolBarLayout;
    QPushButton             *undo;
    QPushButton             *redo;
    QPushButton             *stopSelected;
    QPushButton             *invertSelected;
    QCheckBox               *showSelected;
    QComboBox               *zoom;
    QPushButton             *zoomUp;
    QPushButton             *zoomDown;
    QPushButton             *defaultZoom;
    QComboBox               *rotate;
    QPushButton             *rotateUp;
    QPushButton             *rotateDown;
    QPushButton             *defaultRotate;
    QLabel                  *mirrored;
    QPushButton             *mirrorIt;
    QLabel                  *smoothLabel;
    QComboBox               *strokeSmooth;

    NavigationWidget        *preView;
    QProgressBar*            progressBar;

    // other windows
    NewWindow*    newWindow;
    ConfigWindow* configWindow;
    LogWindow*    logWindow;

    QHash<QString, QAction*> actions;

    // flags
    bool showPanels;

    enum {BASIC_VIEW = 0, FULL_VIEW = 1, FULL_VIEW_ADV  = 2 };
    int  viewMode;


    // constants
    static const int panelsWidthMin = 195;
    static const int panelsWidthMax = 300;

    QHash<QMdiSubWindow*, LayerTreeWidget::Memento> treeMementoMap;
    QMdiSubWindow* lastSubWindow;
};

#endif // MAINWINDOW_H
