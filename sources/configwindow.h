
#ifndef CONFIGWINDOW_H
#define CONFIGWINDOW_H

#include <QWidget>
#include <QStackedWidget>
#include <QPushButton>
#include <QListWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>

#include "config.h"

class ConfigWindow : public QWidget {
    Q_OBJECT
public:
    explicit ConfigWindow(QWidget* parent = 0);
    ~ConfigWindow();

    void setSettingDevice(Settings* device);

signals:
    void settingsApplyed();
    void settingsBeingSetDefault();

public  slots:

private slots:

    void applySettings();
    void setSettingsToDefault();

protected:

private:
    void setupUi();
    void loadConfig();
    void saveConfig();

    Settings* config;

    QListWidget*    groups;
    QStackedWidget* settings;
    QPushButton*    applyButton;
    QPushButton*    closeButton;
    QPushButton*    defaultButton;

    QTableWidget* shortcutsTable;

};

#endif // CONFIGWINDOW_H
