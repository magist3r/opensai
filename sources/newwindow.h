/*
 *   Copyright 2013 Derptech
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * */


#ifndef NEWWINDOW_H
#define NEWWINDOW_H

#include <QWidget>
#include <QFormLayout>
#include <QtGui>



class NewWindow : public QWidget {
    Q_OBJECT
public:

    NewWindow(QWidget *parent = 0)
        : QWidget(parent)
    {
        setObjectName("NewWindow");
        this->setWindowTitle("Create a New Canvas");
        this->setFixedSize(500, 350);
        this->setupUi();
        connect(width,      SIGNAL(editingFinished()), this, SLOT(calcInfo()) );
        connect(heigth,     SIGNAL(editingFinished()), this, SLOT(calcInfo()) );
        connect(resolution, SIGNAL(editingFinished()), this, SLOT(calcInfo()) );
    }
    ~NewWindow(){}

    void setupUi();

public slots:

    void createNewCanvas();

signals:

    void newCanvasCreated(QSize size, QString name);

private slots:

    void calcInfo();

private:

    int wInPls;
    int hInPls;

    QVBoxLayout *commonLayout;
    QFormLayout *layout;

    QLineEdit *name;
    QComboBox *preset;
    QLineEdit *width;
    QLineEdit *heigth;
    QLineEdit *resolution;
    QComboBox *metrics;
    QComboBox *choise;

    // Information
    QGroupBox   *info;
    QFormLayout *infoLayout;
    QLabel      *canvasSize;
    QLabel      *boundWidth;
    QLabel      *boundHeight;

    // Buttons...
    QHBoxLayout *buttsLayout;
    QPushButton *okayButton;
    QPushButton *closeButton;


};

#endif // NEWWINDOW_H
