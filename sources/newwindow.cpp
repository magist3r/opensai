
#include "newwindow.h"

#define _TEST_


void NewWindow::setupUi()
{
    commonLayout = new QVBoxLayout();
    layout       = new QFormLayout();

    commonLayout->setAlignment(Qt::AlignHCenter);
    layout      ->setFormAlignment(Qt::AlignHCenter);

    name = new QLineEdit("New Canvas");
    name->setMaximumWidth(300);
    layout->addRow("Name:", name);

    preset = new QComboBox();
    preset->setMaximumWidth(300);
    preset->addItems(QStringList({ "320 x 240 (QVGA)",
                                   "640 x 480 (VGA)",
                                   "800 x 600 (SVGA",
                                   "1024 x 768 (XGA)",
                                   "1280 x 1024 (SXGA)",
                                   "1400 x 1050 (SXGA+)",
                                   "1600 x 1200 (UXGA)",
                                   "128 x 128",
                                   "256 x 256",
                                   "512 x 512",
                                   "1024 x 1024" } ));

    layout->addRow("Preset:", preset);
    width =  new QLineEdit();
    width->setFixedWidth(150);
    layout->addRow("Width:", width);
    heigth = new QLineEdit();
    heigth->setFixedWidth(150);
    layout->addRow("Heigth:", heigth);

    resolution  = new QLineEdit();
    resolution->setFixedWidth(150);
    layout->addRow("Resolution", resolution);

#ifdef _TEST_
    width->setText("10");
    heigth->setText("10");
    resolution->setText("40");
#endif

    info        = new QGroupBox("Information");
    infoLayout  = new QFormLayout();
    canvasSize  = new QLabel();
    boundWidth  = new QLabel();
    boundHeight = new QLabel();

    infoLayout->setFormAlignment(Qt::AlignHCenter);
    infoLayout->addRow("Canvas Size:", canvasSize);
    infoLayout->addRow("Upper Bound from Width:", boundWidth);
    infoLayout->addRow("Upper Bound from Heigth:", boundHeight);

    info->setLayout(infoLayout);
    commonLayout->addLayout(layout);
    commonLayout->addWidget(info);

    okayButton  = new QPushButton();
    okayButton->setText("OK");
    okayButton->setFixedSize(65, 25);

    connect(okayButton, SIGNAL( clicked() ), this, SLOT( createNewCanvas() ));

    closeButton = new QPushButton();
    closeButton->setFixedSize(65, 25);
    closeButton->setText("Cancel");

    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));

    buttsLayout = new QHBoxLayout();
    buttsLayout->setAlignment(Qt::AlignRight);
    buttsLayout->addWidget(okayButton);
    buttsLayout->addWidget(closeButton);
    commonLayout->addLayout(buttsLayout);

    this->setLayout(commonLayout);

    this->calcInfo();
}

void NewWindow::calcInfo()
{
    wInPls = this->width->text().toInt()  * this->resolution->text().toInt();
    hInPls = this->heigth->text().toInt() * this->resolution->text().toInt();

    this->canvasSize->setText( QString::number(wInPls) + " x " + QString::number(hInPls) );
}
/*
void NewWindow::newCanvasCreated(QSize size)
{
    size.setWidth();
}

*/

void NewWindow::createNewCanvas()
{

    QSize size(wInPls, hInPls);
    /*
    if(name->text().size() == 0)
    {
        QMessageBox msg;
        msg.setText("New Canvas has no name!");
        msg.setWindowIconText("Warning!");
        msg.show();
        return;
    }
    */

    emit newCanvasCreated(size, name->text());
    //QMessageBox msg;
    //msg.show();
    this->close();
}

/*
void NewWindow::testT()
{
    QMessageBox msg;
    msg.exec();
}


*/







