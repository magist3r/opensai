#include "configwindow.h"

#define _TEST_

ConfigWindow::ConfigWindow(QWidget *parent)
    :QWidget(parent)
{
    // load setings....
    setupUi();
    loadConfig();
}

ConfigWindow::~ConfigWindow()
{
    saveConfig();
}

void ConfigWindow::loadConfig()
{
    // shortcuts loading

    return;
}

void ConfigWindow::saveConfig()
{
    // shortcuts saving

    return;
}

void ConfigWindow::setupUi()
{
    setFixedSize(800, 600);
    setWindowIconText("OpenSAI settings");

    QVBoxLayout* layout = new QVBoxLayout();

    QHBoxLayout* menuLayout = new QHBoxLayout();

    groups        = new QListWidget();
    settings      = new QStackedWidget();
    applyButton   = new QPushButton();
    closeButton   = new QPushButton();
    defaultButton = new QPushButton();

    settings->setFixedWidth(width() - width()/4);

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ THEMES TAB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QWidget* themesTab = new QWidget();
    QComboBox* themes = new QComboBox();
    QVBoxLayout* themsTabLayout = new QVBoxLayout();
    themsTabLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    themsTabLayout->addWidget(themes);
    themesTab->setLayout(themsTabLayout);

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SYSTEM TAB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QWidget* systemTab = new QWidget();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INPUT TAB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QWidget* inputTab = new QWidget();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TOOLS TAB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QWidget* toolsTab = new QWidget();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORTCUTS TAB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QWidget* shortcutsTab = new QWidget();
    QVBoxLayout* shortcutsTabLayout = new QVBoxLayout();
    shortcutsTable = new QTableWidget();
    shortcutsTable->setRowCount(1);
    shortcutsTable->setColumnCount(2);
    shortcutsTabLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    shortcutsTabLayout->addWidget(shortcutsTable);
    shortcutsTab->setLayout(shortcutsTabLayout);


    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PLUGINS TAB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QWidget* pluginsTab = new QWidget();

    settings->addWidget(themesTab);
    settings->addWidget(systemTab);
    settings->addWidget(inputTab);
    settings->addWidget(toolsTab);
    settings->addWidget(shortcutsTab);
    settings->addWidget(pluginsTab);

    connect(groups, SIGNAL(currentRowChanged(int)), settings, SLOT(setCurrentIndex(int)));

    groups->addItem("Themes");
    groups->addItem("System");
    groups->addItem("Input");
    groups->addItem("Tools");
    groups->addItem("Shortcuts");
    groups->addItem("Plug-ins");

    menuLayout->addWidget(groups);
    menuLayout->addWidget(settings);

    QHBoxLayout* buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(applyButton);
    buttonsLayout->addSpacing(width());
    buttonsLayout->addWidget(defaultButton);
    buttonsLayout->addWidget(closeButton);
    buttonsLayout->setAlignment(Qt::AlignLeft);

    applyButton  ->setText("Apply");
    closeButton  ->setText("Close");
    defaultButton->setText("Default");

    connect(closeButton,   SIGNAL(clicked()), this, SLOT(close()));
    connect(applyButton,   SIGNAL(clicked()), this, SLOT(applySettings()));
    connect(defaultButton, SIGNAL(clicked()), this, SLOT(setSettingsToDefault()));

    layout->addLayout(menuLayout);
    layout->addLayout(buttonsLayout);
    setLayout(layout);
}


void ConfigWindow::applySettings()
{
    saveConfig();
    emit settingsApplyed();
    return;
}

void ConfigWindow::setSettingsToDefault()
{
    emit settingsBeingSetDefault();
    return;
}


void ConfigWindow::setSettingDevice(Settings* device)
{
    config = device;
}

















