
#include "canvas.h"

Canvas::Canvas(QSize size, QWidget *parent)
{
    setParent(parent);
    setRenderHints(QPainter::Antialiasing);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setBackgroundBrush(Qt::gray);
    setInteractive(true);
    setSceneRect(0, 0, 9000, 9000);

    canvas = QPixmap(size);
    canvas.fill(Qt::white);

    item = new QGraphicsPixmapItem();
    item->setPixmap(canvas);
    item->setPos( (9000 - size.width())/2, (9000 - size.height())/2 );
    scene.addItem(item);
    setScene(&scene);

    // here must be shortcuts setups...
    QShortcut* rotLeft  = new QShortcut(this);
    rotLeft->setKey(QKeySequence(Qt::Key_Delete));
    QObject::connect(rotLeft, SIGNAL(activated()), this, SLOT(rotateLeft()));

    QShortcut* rotRight = new QShortcut(this);
    rotRight->setKey(QKeySequence(Qt::Key_End));
    QObject::connect(rotRight, SIGNAL(activated()), this, SLOT(rotateRight()));

    QShortcut* def = new QShortcut(this);
    def->setKey(QKeySequence(Qt::Key_Home));
    QObject::connect(def, SIGNAL(activated()), this, SLOT(setDefaults()));

    QShortcut* pan = new QShortcut(this);
    pan->setKey(QKeySequence(Qt::Key_Space));
    QObject::connect(pan, SIGNAL(activated()), this, SLOT(panEvent()));

    QShortcut* rot = new QShortcut(this);
    rot->setKey(QKeySequence(tr("Ctrl+Space")));
    QObject::connect(rot, SIGNAL(activated()), this, SLOT(rotorEvent()));
}

Canvas::~Canvas()
{}

void Canvas::repaint(QImage value)
{
    canvas = QPixmap::fromImage(value);
    update();
}

void Canvas::repaint(QPixmap value)
{
    canvas = value;
    update();
}

void Canvas::wheelEvent(QWheelEvent * e)
{
    double deg = e->delta() / 30;
    zoom = (deg < 0.1f)? 0.1f: deg;
    scale(zoom, zoom);
    e->accept();
}

void Canvas::mousePressEvent(QMouseEvent *e)
{
    dots[0] = dots[1] = e->pos();

}

void Canvas::mouseMoveEvent(QMouseEvent *e)
{
    dots[0] = dots[1];
    dots[1] = e->pos();

    canvasEvent();
}

void Canvas::mouseReleaseEvent(QMouseEvent *e)
{
    setCursor(Qt::ArrowCursor);
    mode = MODES::PAINT;
}

void Canvas::canvasEvent()
{
    switch(mode) {
    case MODES::PAINT:
        break;
    case MODES::PAN:
        setCursor(Qt::OpenHandCursor);
        horizontalScrollBar()->setValue(horizontalScrollBar()->value()
                                        - (dots[1].x() - dots[0].x()));

        verticalScrollBar()->setValue(verticalScrollBar()->value()
                                      - (dots[1].y() - dots[0].y()));
        break;
    case MODES::ROTOR:
    {
        setCursor(Qt::OpenHandCursor);

        QPointF center(width()/2, height()/2);
        QPointF A(dots[1].x() - center.x(), dots[1].y() - center.y());
        QPointF B(dots[0].x() - center.x(), dots[0].y() - center.y());

        qreal pScal = A.x() * B.y() - A.y() * B.x();
        qreal scal  = A.x() * B.x() + A.y() * B.y();

        qreal angle = atan2(pScal, scal) * 180 / PI;
        rotate(-angle);
    }
        break;
    default:
        break;
    }
}


void Canvas::panEvent()
{
    mode = MODES::PAN;
}

void Canvas::rotorEvent()
{
    mode = MODES::ROTOR;
}
