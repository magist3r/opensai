#ifndef BASETOOLS_H
#define BASETOOLS_H

#include <QBitmap>
#include <QSettings>
#include <QDir>
#include <QQueue>

#include "tool.h"


/**
 * @brief The Pen class
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
class Pen : public PaintTool {
    Q_OBJECT
public:

    explicit Pen(PaintTool *parent = 0)
        : PaintTool(parent)
    {
        blender = Blending::normal; // as a default
        name = "Pen";
        setupUi();
    }
    ~Pen()
    { }

    virtual bool load(QString name);
    virtual bool save(QString name);
    virtual void setupUi();

    virtual void action(QList<Dot>);
    virtual void action(QList<Dot>, QImage*)  {}

signals:

    void sizeChanged(int newValue);
    void minSizeChanged(int value);
    void densityChanged(int value);

    void blendingChanged(int value);

public slots:

    void setSize(int value);
    void setMinSize(int value);
    void setDensity(int value);

private:

    BlendFunction blender;

    QSlider *blending;

    QFormLayout *layout;
    QVBoxLayout *toolSettings;

};


/**
 * @brief The Binary class
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
class Binary : public PaintTool {
       Q_OBJECT
public:
   explicit Binary(PaintTool *parent = 0)
       : PaintTool(parent)
   {
       dotsPerStroke = 1;
       name = "binary";
       setupUi();
   }
   ~Binary() {}

   virtual bool load(QString name);
   virtual bool save(QString name);
   virtual void setupUi();

   virtual void action(QList<Dot> stroke);
   virtual void action(QList<Dot> stroke, QImage* image);

signals:

   void sizeChanged(int newValue);
   void minSizeChanged(int value);
   void densityChanged(int value);
   void hardnessChanged(int value);
   void sizeByPressChanged();

public slots:

   void setSize(int value);
   void setMinSize(int value);
   void setDensity(int value);
   void setHardness(int value);
   void setSizeByPress();

private:

   QSlider   *opacity;
   QSlider   *hardness;
   QCheckBox *sizeByPressure;

   QFormLayout *layout;
   QVBoxLayout *toolSettings;


};

/**
 * @brief The Flood fill tool
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
class FloodFill : public PaintTool {
    Q_OBJECT
public:
    explicit FloodFill(PaintTool *parent = 0)
        : PaintTool(parent)
    {
        dotsPerStroke = 1;
    }
    ~FloodFill() {}

    virtual bool load(QString name);
    virtual bool save(QString name);
    virtual void setupUi();

    virtual void action(QList<Dot> stroke);
    virtual void action(QList<Dot>, QImage*) {}

 signals:

 public slots:

protected:

    virtual void floodFill(LayerImage *target, QPoint point, QRgb oldColor, QRgb newColor);

 private:


    QFormLayout *layout;
    QVBoxLayout *toolSettings;


};

class EmptyTool : public PaintTool {
    Q_OBJECT
public:
    explicit EmptyTool(PaintTool *parent=0)
        :PaintTool(parent)
    {}
    ~EmptyTool() {}

    virtual bool load(QString name) { Q_UNUSED(name); return true; }
    virtual bool save(QString name) { Q_UNUSED(name); return true; }
    virtual void setupUi() {}

    virtual void action(QList<Dot> stroke, QImage* image)
    {
        QPainter p(image);
        QPen pen;
        pen.setColor(color);
        pen.setWidth(1);
        p.setPen(pen);

        for(Dot& itr : stroke) {
            p.drawPoint(itr.pos);
            p.setOpacity(itr.pressure);
            qDebug(QString::number(itr.pos.rx()).toAscii());
        }


    }
    virtual void action(QList<Dot> stroke)
    {

        Q_UNUSED(stroke);
    }

signals:

public slots:

private:

    QFormLayout *layout;
    QVBoxLayout *toolSettings;

};




#endif // BASETOOLS_H
