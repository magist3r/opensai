#ifndef PATH_H
#define PATH_H

#include <QString>
#include <QStringList>
#include <QObject>
#include <QDir>

class Path : public QObject {
    Q_OBJECT
public:
    explicit Path(QObject* parent = nullptr);
    Path(const Path& value);
    Path(QString path);
    ~Path();

    QString addDir(QString value);
    QString getPath();
    QString moveUp();
    QString fromResources();

protected:
    QStringList subDirs;
};

#endif // PATH_H
