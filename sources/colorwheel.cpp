//C++
#include <cmath>

//Qt
#include <QBrush>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPen>
#include <QResizeEvent>
#include <QtCore/qmath.h>

//Own
#include "colorwheel.h"

ColorWheel::ColorWheel(QWidget *parent) :
    QWidget(parent),
    mouseDown(false),
    inWheel(false),
    inSquare(false),
    Pi(fabs(acos(-1.0))),
    radiusFactor(1.05),
    angleShift(510)
{
    QSize initSize(170, 170);
    setFixedSize(initSize);

    setCursor(Qt::CrossCursor);
}


QColor ColorWheel::colorFromPos(const QPoint &point)
{
    if(inWheel)
    {
        QPoint center(halfSide, halfSide);
        int angle = qRound(atan2(center.y()-point.y(), point.x()-center.x())*180.0/fabs(acos(-1.0)));
        int hue = (-angle + angleShift) % 360;
        return QColor::fromHsv(hue, current.saturation(), current.value() );
    }
    if(inSquare)
    {
        QPoint corner(halfSide - halfSquareSide, halfSide - halfSquareSide);

        QPointF p(lastPos - corner);

        qreal saturation;
        qreal value;

        if (p.x() > squareSide)
            saturation = 1.0;
        else if (p.x() < 0)
            saturation = 0;
        else saturation = p.x()/squareSide;

        if (p.y() > squareSide)
            value = 0;
        else if (p.y() < 0)
            value = 1.0;
        else value = 1.0 - p.y()/squareSide;

        return QColor::fromHsvF(current.hueF(), saturation, value);
    }
    return QColor();
}

QSize ColorWheel::sizeHint () const
{
    int side = qMax(width(), height());
    return QSize(side, side);
}

void ColorWheel::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
    if(wheelRegion.contains(lastPos))
    {
        inWheel = true;
        inSquare = false;
        QColor color = colorFromPos(lastPos);
        setColor(color);
    }
    else if(squareRegion.contains(lastPos))
    {
        inWheel = false;
        inSquare = true;
        QColor color = colorFromPos(lastPos);
        setColor(color);
    }
    mouseDown = true;
}

void ColorWheel::mouseMoveEvent(QMouseEvent *event)
{
    lastPos = event->pos();
    if( !mouseDown ) return;

    QColor color = colorFromPos(lastPos);

    setColor(color);
}

void ColorWheel::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    mouseDown = false;
    inWheel = false;
    inSquare = false;
}

void ColorWheel::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    calculateSizes();
    update();
}

void ColorWheel::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    drawAll();
}

void ColorWheel::calculateSizes()
{
    side = qMin(width(), height());
    halfSide = side/2;
    wheelWidth = qRound(side/11.5);
    margin = qRound(side/34.0);
    radius = halfSide - margin;
    diameter = radius*2;

    colorWheelRadius = radius - wheelWidth/2;
    innerCircleRadius = qRound((radius - wheelWidth)/radiusFactor);
    outerCircleRadius = qRound(radius*radiusFactor);

    //calculate wheel region
    wheelRegion = QRegion(halfSide, halfSide, diameter, diameter, QRegion::Ellipse);
    wheelRegion.translate(-radius, -radius);

    int subRegSize = diameter - 2*wheelWidth;
    int subRegHalfSize = radius - wheelWidth;
    QRegion subReg(halfSide, halfSide, subRegSize, subRegSize, QRegion::Ellipse );
    subReg.translate(-subRegHalfSize, -subRegHalfSize);
    wheelRegion -= subReg;

    halfSquareSide = qRound(innerCircleRadius/qSqrt(2.0));
    squareSide = 2*halfSquareSide;

    squareRegion = QRegion(halfSide, halfSide, squareSide, squareSide);
    squareRegion.translate(-halfSquareSide, -halfSquareSide);
}

void ColorWheel::preparePainter(QPainter& painter)
{
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(halfSide, halfSide); // (0,0) is central point of wheel
}

void ColorWheel::drawAll()
{
    drawWheel();
    drawSquare();
    drawIndicator();
    drawPicker();
}

void ColorWheel::drawWheel()
{
    QPainter painter;
    preparePainter(painter);

    QConicalGradient conicalGradient(0, 0, 0);

    for (int angle=0; angle<=360; angle+=30)
    {
        conicalGradient.setColorAt(angle/360.0, QColor::fromHsv((-angle + angleShift) % 360, 0xFF, 0xFF));
    }

    //draw color circle
    painter.setPen(QPen(QBrush(conicalGradient), wheelWidth));
    painter.setBrush(Qt::NoBrush);
    painter.drawEllipse(QPoint(0,0), colorWheelRadius, colorWheelRadius);

    //inner and outer bound circles
    painter.setPen(QColor(Qt::gray));
    painter.setBrush(Qt::NoBrush);
    painter.drawEllipse(QPoint(0,0), innerCircleRadius, innerCircleRadius);
    painter.drawEllipse(QPoint(0,0), outerCircleRadius, outerCircleRadius);
}

void ColorWheel::drawSquare()
{
    QPainter painter;
    preparePainter(painter);

    QImage square(255,255,QImage::Format_ARGB32_Premultiplied);
    for(int i=0; i<square.height(); ++i)
    {
        QRgb* pixelLine = reinterpret_cast<QRgb*>(square.scanLine(i));
        for(int j=0; j<square.width(); ++j)
        {
            QColor color = QColor::fromHsv(current.hue(),j,255-i);
            pixelLine[j] = color.rgb();
        }
    }

    square = square.scaled(squareSide, squareSide);

    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::SolidPattern);
    painter.drawImage(-halfSquareSide, -halfSquareSide, square);
}

void ColorWheel::drawIndicator()
{
    QPainter painter;
    preparePainter(painter);

    const int hue = current.hue();
    QColor opColor = QColor::fromHsv((hue+180)%360, 0xFF, 0xFF); //opposite color
    painter.setPen(QPen(QBrush(opColor), 2.0));
    painter.setBrush(Qt::NoBrush);

    qreal angle = ((-hue + angleShift) % 360)/180.0*Pi;
    QPointF center(colorWheelRadius*cos(angle), -colorWheelRadius*sin(angle));
    painter.drawEllipse(center, 4, 4);
}

void ColorWheel::drawPicker()
{
    QPainter painter;
    preparePainter(painter);

    QColor opColor = QColor::fromHsv(0, 0, 0xFF - current.value());
    painter.setPen(QPen(QBrush(opColor), 2.0));
    painter.translate(-halfSquareSide, -halfSquareSide);

    QPointF center(current.saturationF()*squareSide, squareSide - current.valueF()*squareSide);
    painter.drawEllipse(center, 4, 4);
}

void ColorWheel::setColor(QColor color)
{
    if (current != color)
    {
        current = color;
        update();
        emit colorChanged(color);
    }
}

bool ColorWheel::event(QEvent *event)
{
    if (event->type() == QEvent::ToolTip)
    {
        QHelpEvent* ttEvent = static_cast<QHelpEvent*>(event);
        QPoint pos = ttEvent->pos();
        if (wheelRegion.contains(pos))
        {
            setToolTip("Hue Wheel");
        }
        else if (squareRegion.contains(pos))
        {
            setToolTip("Saturation / Value Selector");
        }
        else
        {
            setToolTip("");
        }
    }
    return QWidget::event(event);
}
