#include "layer.h"


Layer::Layer() : img(nullptr)
{

}

Layer::Layer(QImage image) : img(nullptr)
{
    setImage(image);
}

Layer::Layer(QImage image, Blending::BlendFunction blendMode) : img(nullptr)
{
    setImage(image);
    setBlendMode(blendMode);
}

Layer::~Layer()
{
    if (img)
    {
        delete img;
    }
}

Layer* Layer::myLayerPointer()
{
    return this;
}

LayerSet* Layer::myLayerSetPointer()
{
    Q_ASSERT_X(false, "Layer::myLayerSetPointer", "Attempt to cast from Layer to LayerSet");
    return nullptr;
}

bool Layer::isLayer() const
{
    return true;
}

void Layer::setImage(QImage image)
{
    if (img)
        *img = image;
    else
        img = new LayerImage(image);
}

LayerImage* Layer::Layer::image() const
{
    return img;
}
