#ifndef CONFIG_H
#define CONFIG_H

#include <QVariant>
#include <QVariantMap>
#include <QObject>
#include <QString>
#include <QHash>
#include <QMap>
#include <QStringList>
#include <QDir>

#include "path.h"
#include "logger.h"

#define CONFIG Settings::getInstance()

typedef QMap<QString, QVariant> Config;
typedef QMap<QString, Config> ConfigGlobal;

class Settings : public QObject {
    Q_OBJECT
public:
    ~Settings();

    static Settings& getInstance()
    {
        static Settings instance;
        return instance;
    }

    bool loadConfig(QString name);
    bool saveConfig(QString path);

    void setFileType(QString value);

    QVariant& operator()(const QStringList index)
    {
        return data[index[0]][index[1]];
    }

    Settings(const Settings &) = delete;
    Settings &operator= (const Settings &) = delete;

private:

    Config loadFile(QString name);
    void   saveFile(QString name, Config value);


    Settings();

    ConfigGlobal data;

    QString FILE_TYPE = "conf";

};

#endif // CONFIG_H
