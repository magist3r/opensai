#ifndef COLORPAIR_H
#define COLORPAIR_H

#include <QWidget>
#include <QPixmap>
#include <QPainter>
#include <QShortcut>
#include <QFrame>
#include <QPushButton>
#include <QDir>

class ColorRect : public QFrame {
    Q_OBJECT
public:
    explicit ColorRect(QFrame *parent = 0);
    ~ColorRect();

signals:
    void colorChanged(QColor value);

public slots:
    void   setColor(QColor value);
    QColor getColor();

protected:
    virtual void paintEvent(QPaintEvent *e) override;

private:
    QColor color;
};

class ColorPair : public QWidget {
    Q_OBJECT
public:
    explicit ColorPair(QWidget *parent = 0);
    ~ColorPair();

    QColor getPrimaryColor();
    QColor getSecondaryColor();

signals:
    void primaryColorChanged(QColor value);
    void secondaryColorChanged(QColor value);
    void colorsSwaped();

public slots:
    void setPrimaryColor(QColor value);
    void setSecondaryColor(QColor value);
    void swapColors();
    void setShortCut(QKeySequence key);

private:

    ColorRect   *primaryColor;
    ColorRect   *secondaryColor;
    QPushButton *swapButt;
    QShortcut   *swapper;

    static constexpr qreal ATTITUDE = 0.63;
};



#endif // COLORPAIR_H
