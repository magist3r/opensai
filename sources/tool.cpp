/*
 *   Copyright 2013 Derptech
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * */


#include "tool.h"

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DOT STRUCT IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~*/

Dot::Dot() {}
Dot::Dot(QPointF _pos, qreal _press, QPointF _tilt, qreal _tang, qreal _angle)
    : pos(_pos), pressure(_press), tilt(_tilt), tangental(_tang), angle(_angle)
{}
Dot::~Dot(){}

Dot Dot::operator+(const Dot& arg)
{
    return { pos + arg.pos, pressure + arg.pressure,
        tilt + arg.tilt, tangental + arg.tangental, angle + arg.angle };
}

Dot Dot::operator*(const double& arg )
{
    return { pos * arg, pressure * arg, tilt * arg, tangental * arg, angle * arg };
}

Dot& Dot::operator+=(Dot arg )
{
    pos += arg.pos;
    pressure += arg.pressure;
    tilt += arg.tilt;
    tangental += arg.tangental;
    angle += arg.angle;
    return *this;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TOOL BASE IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~*/

void ToolBase::setLayer(QImage *layer)
{
    //Q_ASSERT(layer == nullptr);
    this->layer = layer;
    emit layerChanged(layer);
}

void ToolBase::setColor(QColor value)
{
    color = value;
}


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PAINT TOOL IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~*/

