#include "rgbwidget.h"
#include "adjuvantwidgets.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>

RgbWidget::RgbWidget(QWidget *parent) :
    QWidget(parent)
{   
    redSlider = new ColorSlider('r');
    greenSlider = new ColorSlider('g');
    blueSlider = new ColorSlider('b');

    Frame* rFrame = new Frame();
    Frame* gFrame = new Frame();
    Frame* bFrame = new Frame();

    QVBoxLayout* rLay = new QVBoxLayout();
    QVBoxLayout* gLay = new QVBoxLayout();
    QVBoxLayout* bLay = new QVBoxLayout();
    rLay->setSpacing(2);
    rLay->setMargin(2);

    gLay->setSpacing(2);
    gLay->setMargin(2);

    bLay->setSpacing(2);
    bLay->setMargin(2);

    rFrame->setLayout(rLay);
    gFrame->setLayout(gLay);
    bFrame->setLayout(bLay);

    rLay->addWidget(redSlider);
    gLay->addWidget(greenSlider);
    bLay->addWidget(blueSlider);

    ColorSlider* sliders[] = { redSlider, greenSlider, blueSlider };
    Frame* frames[] = { rFrame, gFrame, bFrame };
    const char* shortNames[] = { "R", "G", "B"};
    const char* longNames[] = {"Red Slider", "Green Slider", "Blue Slider" };

    QLabel standard("000"); //some kind of hack
    standard.resize(standard.sizeHint());

    QGridLayout* grid = new QGridLayout();
    for (int i=0; i<3; ++i)
    {
        ColorSlider* slider = sliders[i];
        Frame* frame = frames[i];
        slider->setObjectName(longNames[i]);
        slider->setToolTip(longNames[i]);
        slider->setRange(0, 255);

        QLabel* nameLabel = new QLabel(shortNames[i]);
        QLabel* valLabel = new QLabel();
        valLabel->setFixedWidth(standard.width());
        valLabel->setAlignment(Qt::AlignRight);

        connect(slider, SIGNAL(valueChanged(int)), valLabel, SLOT(setNum(int)));
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(emitColorChanged(int)));

        connect(this, SIGNAL(colorChanged(QColor)), slider, SLOT(setCommonColor(QColor)));

        grid->addWidget(nameLabel, i, 0);
        //grid->addWidget(slider,    i, 1);
        grid->addWidget(frame, i, 1);
        grid->addWidget(valLabel,  i, 2);
    }

    setLayout(grid);
}

void RgbWidget::setColor(QColor color)
{
    if (color != QColor(redSlider->value(), greenSlider->value(), blueSlider->value()))
    {
        redSlider->setValue(color.red());
        greenSlider->setValue(color.green());
        blueSlider->setValue(color.blue());

        emit colorChanged(color);
    }
}

void RgbWidget::emitColorChanged(int)
{
    emit colorChanged(QColor(redSlider->value(), greenSlider->value(), blueSlider->value()));
}
