//Own
#include "layerimage.h"


LayerImage::LayerImage(int w, int h) : QImage(w, h, QImage::Format_ARGB32)
{

}

LayerImage::LayerImage(const QImage &img) : QImage(img)
{

}

LayerImage::LayerImage(const LayerImage &li) : QImage(li)
{

}

bool LayerImage::hasColorAt(int x, int y) const
{
    return pixel(x, y) != 0u;
}

QImage LayerImage::renderWithAlpha(QColor alphaColor) const
{
    QImage toReturn(this->size(), this->format());
    const QRgb alphaRgb = alphaColor.rgb();
    for (int i=0; i<this->height(); ++i)
    {
        const QRgb* originalLine = reinterpret_cast<const QRgb*>(this->scanLine(i));
        QRgb* resultLine = reinterpret_cast<QRgb*>(toReturn.scanLine(i));

        for (int j=0; j<this->width(); ++j)
        {
            resultLine[j] = (originalLine[j] == 0u) ? alphaRgb : originalLine[j];
        }
    }
    return toReturn;
}
