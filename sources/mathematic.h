#ifndef MATHEMATIC_H
#define MATHEMATIC_H

#include <vector>
#include <cmath>


namespace math {

/**
 *
 */
template< template <typename> class T, typename C >
class BezierCurve {
	typedef  C point;
	typedef  T<point> path;
public:
	BezierCurve() {

	}
	~BezierCurve() {}

	point getBezierCurvePoint(path p, double t);
	path  getBezierCurve(path p);

private:
	long long fact(long value);
	long long binom(int i, int n);
	double    bPolynom(int i, int n, double t);

};




} // namespace math


#endif // MATHEMATIC_H
