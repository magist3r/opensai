#ifndef LAYERTREEWIDGET_H
#define LAYERTREEWIDGET_H

//Qt
#include <QImage>
#include <QHash>
#include <QList>
#include <QPixmap>
#include <QSize>
#include <QString>
#include <QWidget>

//Own
#include "layerset.h"
#include "layerframe.h"

class LayerSet;
class LayerFrame;
class QSpacerItem;
class QVBoxLayout;

class LayerTreeWidget : public QWidget
{
    Q_OBJECT
public:

    struct Memento
    {
        friend class LayerTreeWidget;

        Memento() : layerSize(0,0), selected(nullptr), layerCounter(0), setCounter(0)
        {

        }

        Memento(const Memento& other) :
            layerSize(other.layerSize), upperLevelLayers(other.upperLevelLayers), selected(other.selected),
            layerCounter(other.layerCounter), setCounter(other.setCounter)
        {

        }

        uint hash() const
        {
            return qHash(layerCounter + setCounter) + qHash(selected);
        }

        void release();

    private:
        QSize layerSize;
        QList<LayerFrame*> upperLevelLayers;
        LayerFrame* selected;

        int layerCounter;
        int setCounter;
    };

    explicit LayerTreeWidget(QWidget *parent = 0);
    ~LayerTreeWidget();

    void init(QSize size);
    void resetToDefault();

    void setLayerSize(QSize size) {
        _layerSize = size;
    }

    Memento toMemento();
    void fromMemento(const Memento& memento);

    void insertLayerAfter(LayerFrame* toInsert, LayerFrame* other);
    void insertLayerBefore(LayerFrame* toInsert, LayerFrame* other);
    void insertLayerIntoSet(LayerFrame* toInsert, LayerFrame* set);

    QSize layerSize() const {
        return _layerSize;
    }

    AbstractLayer *getCurrentLayerFrame();

signals:
    void debugInfoUpdated(QString);
    void imageRendered(QImage);
    void imageRendered(QPixmap);

    void blendModeChanged(QString blendMode);
    
public slots:
    void updateDebugInfo();
    void renderImage();

    void addNewLayer();
    void addNewLayerSet();
    void removeSelected();

    void wasSelected();
    void setSelected(LayerFrame *layerFrame);

    void setBlendModeForSelected(QString blendMode);

private:
    void detachLayer(LayerFrame* layerFrame);
    void reconstructVBox();
    void addLayerImpl(LayerFrame *layerFrame);

    void insertUpperLayer(LayerFrame* layerFrame, int ind);
    void removeUpperLayer(LayerFrame* layerFrame);

    QList<LayerFrame*> getAllLayerFrames();

    QVBoxLayout* _vbox;
    LayerFrame* _selected; //current selected LayerFrame

    int _layerCounter;
    int _setCounter;

    QList<LayerFrame*> _upperLevelLayers;

    QSize _layerSize;
    LayerSet* _upperLayerSet;
};

uint qHash(const LayerTreeWidget::Memento& memento);

#endif // LAYERTREEWIDGET_H
