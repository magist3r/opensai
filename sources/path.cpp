
#include "path.h"

Path::Path(QObject *parent)
    :QObject(parent)
{
}

Path::Path(const Path &value)
{
    subDirs = value.subDirs;
}

Path::Path(QString path)
{
    subDirs = path.split("/", QString::KeepEmptyParts);
}

Path::~Path()
{

}

QString Path::addDir(QString value)
{
    subDirs.push_back(value);
    return QDir::toNativeSeparators(/*"/" + */subDirs.join("/"));
}

QString Path::getPath()
{
    return QDir::toNativeSeparators(/*"/" +*/ subDirs.join("/"));
}

QString Path::moveUp()
{
    subDirs.pop_back();
    return QDir::toNativeSeparators(/*"/" + */subDirs.join("/"));
}

QString Path::fromResources()
{
    return QDir::toNativeSeparators(/*":" + */subDirs.join("/"));
}
