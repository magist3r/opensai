
#include "adjuvantwidgets.h"

//#define TEST_

ImageRadioGroup::ImageRadioGroup(QWidget *parent)
    : QWidget(parent)
{
    buttons = new QList<QPushButton*>();
    setupUi();
}

void ImageRadioGroup::setupUi()
{
    layout = new QHBoxLayout(this);
    // some work

    setLayout(layout);
}


void ImageRadioGroup::addButton(QIcon icon)
{
    QPushButton *btt = new QPushButton(this);
    btt->setIcon(icon);
    btt->setFixedSize(25, 25);
    buttons->push_back(btt);
    layout->addWidget(btt);
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ NAVIGATION WIDGET IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

void NavigationWidget::rotateUp(qreal value)
{
    angle += value;
    emit rotated(value);
    emit rotated(int(value));
    update();
}

void NavigationWidget::scaleUp(qreal value)
{
    scale += value;
    emit scaled(value);
    emit scaled(int(value));
    update();
}

void NavigationWidget::moveUp(QPointF value)
{
    bufferPos += value;
    emit moved(value);
    emit moved((QPointF)value);
    update();
}

void NavigationWidget::setImage(QPixmap value)
{
    buffer = value;
    update();
}

NavigationWidget::NavigationWidget(QWidget *parent)
    : QWidget(parent)
{
    setPalette(Qt::gray);
    setMinimumSize(150, 150);
    setAutoFillBackground(true);
    //buffer = nullptr;
    scale = 1.0f;
    angle = 0.0f;

    mousePressed = false;
    bufferPos = {0, 0};
    viewPortSize = {0, 0};

#ifdef TEST_
    buffer = new QImage(40, 160, QImage::Format_ARGB32);
    buffer->fill(Qt::white);
    bufferPos =  QPointF(4, 4);
    angle = 0.0f;
    scale = 1.0f;
#endif

}

NavigationWidget::~NavigationWidget()
{

}


void NavigationWidget::mouseMoveEvent(QMouseEvent *e)
{
    points[1] = points[0];
    points[0] = e->pos();
    moveUp(points[0] - points[1]);
#ifdef TEST_
    qDebug((QString::number(points[0].rx()).toStdString()
            + " " + QString::number(bufferPos.rx()).toStdString()).c_str() );
#endif
    update();
}

void NavigationWidget::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton){
        mousePressed = true;
    }
    points[1] = points[0] =  e->pos();
}

void NavigationWidget::mouseReleaseEvent(QMouseEvent *)
{
    mousePressed = false;
}

void NavigationWidget::paintEvent(QPaintEvent *)
{
    //if(buffer != nullptr) {
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);

        qreal difX  = buffer.width()  / width();
        qreal difY  = buffer.height() / height();
        qreal delta = (difX >= difY)? difX : difY;
        QRect rect;
        rect.setTopLeft(QPoint( (width() - buffer.width() / delta) / 2,
                                (height() - buffer.height() / delta  ) /2) );
        rect.setWidth ( buffer.width() / delta );
        rect.setHeight( buffer.height() / delta );

        painter.drawPixmap( rect, buffer );

        // Painting the navigation rectangle

        QPen pen(Qt::red);
        pen.setStyle(Qt::DashLine);

        navi = QRect(QPoint(0, 0), QSize(width()-1, height()-1 ));
        QPixmap naviMap(navi.size() + QSize(1, 1));
        naviMap.fill(Qt::transparent);

        QPainter dr(&naviMap);
        dr.setPen(pen);
        dr.drawRect(navi);


        QTransform trans;
        trans.scale(scale, scale);
        trans.rotate(angle);
        QPixmap transd = naviMap.transformed(trans);

        QPainter naviDraw(this);
        naviDraw.drawPixmap((naviMap.width()  - transd.width() ) / 2.0 + bufferPos.rx(),
                            (naviMap.height() - transd.height()) / 2.0 + bufferPos.ry(),
                             transd);
   // }
}

void NavigationWidget::resizeEvent(QResizeEvent *)
{
    update();
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TOOL_ITEM IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

ToolsItem::ToolsItem(QFrame *parent)
   :QFrame(parent)
{
   setFrameStyle(QFrame::Box);
   setLineWidth(1);
   layout.setMargin(0);
   layout.setSpacing(1);
   setLayout(&layout);
   //setAutoFillBackground(true);
   //QPalette pal = palette();
   //pal.setBrush(QPalette::Background, QColor(51, 51, 51));
   //setPalette(pal);
}

ToolsItem::ToolsItem(ToolBase *tool, QFrame *parent)
   :ToolsItem(parent)
{
   setTool(tool);
}

ToolsItem::~ToolsItem()
{

}


void ToolsItem::setTool(ToolBase *value)
{
   tool = value;
   image.setPixmap(QPixmap(QDir::toNativeSeparators(".//config//tools//" + tool->getName())));
   name.setText(tool->getName());
   layout.addWidget(&image);
   layout.addWidget(&name);
}
ToolBase* ToolsItem::getTool()
{
    return nullptr;
}


void ToolsItem::mousePressEvent(QMouseEvent *)
{
   emit clicked(tool);
}


ToolsGridWidget::ToolsGridWidget(QWidget *parent)
   : QWidget(parent)
{
   columns  = 4;
   rows     = 10;
   setMinimumSize(187, 500); // kinda ugly :(
   layout.setMargin(0);
   layout.setSpacing(0);
   layout.setAlignment(Qt::AlignLeft | Qt::AlignTop);
   setLayout(&layout);
   setAutoFillBackground(true);
   QPalette pal = palette();
   pal.setBrush(QPalette::Background, QColor(51, 51, 51)); // this's shit!
   setPalette(pal);
}

ToolsGridWidget::~ToolsGridWidget()
{

}

void ToolsGridWidget::addTool(int row, int col, ToolBase *value)
{
   ToolsItem *toolItem = new ToolsItem(value);
   toolItem->setFixedSize(width()/columns, height()/rows);
   tools.push_back(new ToolsItem(value));
   layout.addWidget(toolItem, row, col);
   connect(toolItem, SIGNAL(clicked(ToolBase*)), this, SIGNAL(selectedTool(ToolBase*)));
}

void ToolsGridWidget::addTool(ToolBase *value)
{
   ToolsItem *toolItem = new ToolsItem(value);
   toolItem->setFixedSize(width()/columns, height()/rows);
   tools.push_back(new ToolsItem(value));
   layout.addWidget(toolItem);
   connect(toolItem, SIGNAL(clicked(ToolBase*)), this, SIGNAL(selectedTool(ToolBase*)));
}

void ToolsGridWidget::mousePressEvent(QMouseEvent *) {}
void ToolsGridWidget::paintEvent(QPaintEvent *event)
{
    QPainter paint(this);
    QPen pen(Qt::gray);
    paint.setPen(pen);
    paint.drawRect(0, 0, width()-1, height()-1);

    int stepX = width()/columns;
    int stepY = height()/rows;
    for(int i=0; i < columns; ++i){
        paint.drawLine(i  *stepX, 0, i * stepX, height());
    }
    for(int i=0; i < rows; ++i){
        paint.drawLine(0, i * stepY, width(), i * stepY);
    }
    //QWidget::paintEvent(event);
    Q_UNUSED(event);
}

void ToolsGridWidget::resizeEvent(QResizeEvent *)
{
   update();
}

























