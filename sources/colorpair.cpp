#include "colorpair.h"

ColorRect::ColorRect(QFrame *parent)
    : QFrame(parent)
{
    setFrameStyle(QFrame::Box);
    setLineWidth(1);
    setAutoFillBackground(true);
}

ColorRect::~ColorRect()
{

}

QColor ColorRect::getColor()
{
    return color;
}

void ColorRect::setColor(QColor value)
{
    color = value;
    update();
    emit colorChanged(value);
}

void ColorRect::paintEvent(QPaintEvent* e)
{
    QPixmap map(width(), height());
    map.fill(color);

    QPainter p(this);
    p.drawPixmap(2, 2, width()-4, height()-4, map );
    QFrame::paintEvent(e);
}


//~~~~~~~~~~~~~~~~~~ COLOR PAIR WIDGET IMPLEMENTATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ColorPair::ColorPair(QWidget *parent)
    :QWidget(parent)
{
    setFixedSize(45, 45);
    secondaryColor = new ColorRect();
    primaryColor   = new ColorRect();
    secondaryColor->setParent(this);
    primaryColor  ->setParent(this);

    qreal diffX = width() * ATTITUDE;
    qreal diffY = height() * ATTITUDE;
    secondaryColor->setGeometry(width() - diffX, height() - diffY, diffX, diffY);
    primaryColor  ->setGeometry(0, 0, diffX, diffY);

    swapper = new QShortcut(QKeySequence(tr("X")), this);
    connect(swapper, SIGNAL(activated()), this, SLOT(swapColors()) );

    // test only
    primaryColor->setColor(QColor(134, 159, 171));
    secondaryColor->setColor(QColor(34, 53, 62));
    // test only


    swapButt = new QPushButton(this);
    swapButt->setGeometry(diffX + 1, 1, width() - diffX - 2, height() - diffY - 2);
    connect(swapButt, SIGNAL(clicked()), this, SLOT(swapColors()) );
    //swapButt->setIcon(QIcon(QDir::toNativeSeparators(".//skins//basic//images//swap.png")));
    //swapButt->setIcon(QIcon(QDir::toNativeSeparators("C://swap.png")));

}

ColorPair::~ColorPair()
{

}

void ColorPair::setPrimaryColor(QColor value)
{
    primaryColor->setColor(value);
    emit primaryColorChanged(value);
}

void ColorPair::setSecondaryColor(QColor value)
{
    secondaryColor->setColor(value);
    emit secondaryColorChanged(value);
}

void ColorPair::setShortCut(QKeySequence key)
{
    swapper->setKey(key);
}

void ColorPair::swapColors()
{
    QColor tmp = primaryColor->getColor();
    primaryColor  ->setColor(secondaryColor->getColor());
    secondaryColor->setColor(tmp);
    emit colorsSwaped();
    emit primaryColorChanged(primaryColor->getColor());
}

QColor ColorPair::getPrimaryColor()
{
    return  primaryColor->getColor();
}

QColor ColorPair::getSecondaryColor()
{
    return secondaryColor->getColor();
}







