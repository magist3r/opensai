
#include "pluginmanager.h"

template<typename T>
PluginManager<T>::PluginManager()
{

}

template<typename T>
PluginManager<T>::~PluginManager()
{
    for(PluginType itr: plugins) {
        itr->unload();
    }
}

template<typename T>
typename PluginManager<T>::PluginType PluginManager<T>::loadPlugin(QString name)
{
    Plugin toolLib;
    if(QLibrary::isLibrary(name)) {
        toolLib.setFileName(name);
        if(!toolLib.load())
            return nullptr;

        return qobject_cast<PluginType>(toolLib.instance());
    } else {
        return nullptr;
    }
}

template<class T>
bool PluginManager<T>::loadAllPlugins(QString dirName)
{
    Path path(dirName);
    QDir localDir(path.getPath());
    QStringList pluginsList = localDir.entryList(QDir::Files | QDir::NoDotAndDotDot);

    bool result = false;
    for(QString itr : pluginsList) {
        path.addDir(itr);
        PluginType entry = loadPlugin(path);

        if(entry != nullptr)
            result |= true;
        else
            plugins.push_back(entry);
        path.moveUp();
    }
    return result;
}


template<typename T>
QVector<typename PluginManager<T>::PluginType>& PluginManager<T>::getPlugins()
{
    return this->plugins;
}

