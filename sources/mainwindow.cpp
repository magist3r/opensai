

#include "mainwindow.h"
#include "blendmode.h"
#include "layerframe.h"

#include <QSizePolicy>

void  MainWindow::keyPressEvent(QKeyEvent *event) {
    Q_UNUSED(event);
}

MainWindow::MainWindow()
    : lastSubWindow(nullptr)
{
    min_width = 900;
    min_heigth = 600;
    this->setMinimumSize(min_width, min_heigth);
    this->setWindowTitle("OpenSAI/Kukri ");
    this->setFocusPolicy(Qt::ClickFocus);


    LogFile* logErr = new LogFile(stderr);
    logWindow = new LogWindow();

    logIt.addDevice(logErr);
    logIt.addDevice(logWindow);
    logIt << Logger::Info << "Applejack is best pony!" << " Carrot Top is better!" << Logger::End;
    logIt << Logger::Error << "Just Test" << Logger::End;

    showPanels = true;
    viewMode   = BASIC_VIEW;
    setupUi();

}

MainWindow::~MainWindow()
{

}

void MainWindow::createNewCanvas(QSize size, QString name)
{
    QMdiSubWindow* mdiWindow = new QMdiSubWindow(this);
    Canvas *canv = new Canvas(size);
    //canv->setMdiWindow(mdiWindow);
    mdiWindow->setWidget(canv);
    mdiWindow->setAttribute(Qt::WA_DeleteOnClose);
    mdiWindow->setWindowTitle(name);
    mdiWindow->setPalette(Qt::gray);
    mdiWindow->resize(size.width() + 5, size.height()+5);

    if (!panelRight->isEnabled()) {
        setLayersEnabled(true);
    }

    connect(canv, SIGNAL(aboutToClose(QMdiSubWindow*)), this, SLOT(subWindowDestroyedSlot(QMdiSubWindow*)));

    connect(canv,      SIGNAL(doAction(QList<Dot>)),   this,       SLOT(doAction(QList<Dot>)));
    connect(layerTree, SIGNAL(imageRendered(QPixmap)), this,       SIGNAL(canvasUpdate(QPixmap)));
    connect(this,      SIGNAL(canvasUpdate(QPixmap)),  canv,       SLOT(reDraw(QPixmap)));
    connect(this,      SIGNAL(canvasUpdate(QPixmap)),  preView,    SLOT(setImage(QPixmap)));
    connect(canv,      SIGNAL(focused()),              wAreaFrame, SLOT(setFocus()));

    workArea->addSubWindow( mdiWindow );

    mdiWindow->show();

    layerTree->init(size);

    // Navigation-preview widget
    //preView->setImage(canv->getImage());

    emit newCanvasCreated(size, name);
}

void MainWindow::hidePanels() {
    if(!showPanels){
        panelLeft ->setVisible( false );
        panelRight->setVisible( false );
        toolBar   ->setVisible( false );
        statusBar ->setVisible( false );
    } else {
        panelLeft ->setVisible( true );
        panelRight->setVisible( true );
        toolBar   ->setVisible( true );
        statusBar ->setVisible( true );
    }
    showPanels = ! showPanels;

}

void MainWindow::setFullScreen()
{
    viewMode++;
    switch(viewMode) {
    case FULL_VIEW:
        showFullScreen();
        break;
    case FULL_VIEW_ADV:
        menu->setVisible( false );
        showFullScreen();
        center->showFullScreen();
        break;
    default:
        menu->show();
        showMaximized();
        center->showNormal();
        viewMode = 0;
        break;
    }
}

void MainWindow::setupMenu()
{
    // realy needs to run it before
    // because it's settings of this program!
    configWindow = new ConfigWindow();
    connect(configWindow, SIGNAL(settingsApplyed()), this, SLOT(applySettings()));

    menu = new QMenuBar;
    menu->setFocusPolicy(Qt::ClickFocus);


    /* ~~~~~~~~~~~~~~~~~~~~~ FILE MENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    QMenu *menuFile = new QMenu("File");
    menuFile->setFocusPolicy(Qt::ClickFocus);
    newWindow = new NewWindow();
    //connect(newWindow, SIGNAL(newCanvasCreated(QSize)), this, SLOT(createNewCanvas(QSize)));
    connect(newWindow, SIGNAL(newCanvasCreated(QSize, QString)), this, SLOT(createNewCanvas(QSize, QString)) );


    QAction *newAct = new QAction(QIcon(""), tr("&New"), this);
    newAct->setShortcut(QKeySequence(CONFIG({"shortcuts", "New Canvas"}).toString()));
    actions["New Canvas"] = newAct;
    connect(newAct, SIGNAL(triggered()), newWindow, SLOT(show()));
    menuFile->addAction(newAct);


    menuFile->addAction("Open");
    menuFile->addAction("Create from clipboard");
    menuFile->addMenu("Open recent");
    menuFile->addSeparator();
    menuFile->addAction("Save");
    menuFile->addAction("Save as");
    menuFile->addMenu("Export as");
    menuFile->addSeparator();
    menuFile->addAction("Close");
    menuFile->addSeparator();

    QAction *exitAct = new QAction(QIcon(""), tr("&Exit"), this);
    exitAct->setShortcuts(QKeySequence::Close);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
    menuFile->addAction(exitAct );
    menu->addMenu(menuFile);

    /* ~~~~~~~~~~~~~~~~~~~~~ EDIT MENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    QMenu *menuEdit = new QMenu("Edit");
    menuEdit->addAction("Undo");
    menuEdit->addAction("Redo");
    menuEdit->addSeparator();
    menuEdit->addAction("Cut");
    menuEdit->addAction("Copy");
    menuEdit->addAction("Paste");
    menuEdit->addSeparator();
    menuEdit->addAction("Copy by Selection");
    menu->addMenu(menuEdit);

    /* ~~~~~~~~~~~~~~~~~~~~~ CANVAS MENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    QMenu *menuCanvas = new QMenu("Canvas");
    menuCanvas->addAction("Change Resolution");
    menuCanvas->addAction("Change Size");
    menuCanvas->addSeparator();
    menuCanvas->addAction("Crop by Selection");
    menuCanvas->addSeparator();
    menuCanvas->addAction("Flip Horizontally");
    menuCanvas->addAction("Flip Vertically");
    menuCanvas->addAction("Rotate 90° Counter-clockwise");
    menuCanvas->addAction("Rotate 90° Clockwise");
    menu->addMenu(menuCanvas);

    /* ~~~~~~~~~~~~~~~~~~~~~ LAYER MENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    QMenu *menuLayer = new QMenu("Layer");
    menu->addMenu(menuLayer);
    QMenu *menuSelection = new QMenu("Selection");
    menu->addMenu(menuSelection);
    QMenu *menuFilter = new QMenu("Filter");
    menu->addMenu(menuFilter);
    QMenu *menuView = new QMenu("View");
    menu->addMenu(menuView);

    /* ~~~~~~~~~~~~~~~~~~~ WINDOW MENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QMenu *menuWindow = new QMenu("Window");
    QAction* logWindowAction = new QAction("Log window", menuWindow);
    connect(logWindowAction, SIGNAL(changed()), logWindow, SLOT(show()));
    menuWindow->addAction(logWindowAction);
    menu->addMenu(menuWindow);    

    /* ~~~~~~~~~~~~~~~~~~~~~ OTHERS MENU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    QMenu *menuOthers = new QMenu("Others");
    menuOthers->addAction("Help");
    menuOthers->addSeparator();
    QAction* configOpenAction = new QAction("Settings", menu);
    connect(configOpenAction, SIGNAL(triggered()), configWindow, SLOT(show()));
    menuOthers->addAction(configOpenAction);

    menuOthers->addSeparator();
    menuOthers->addAction("About");
    menu->addMenu(menuOthers);

    /* ~~~~~~~~~~~~~~~~~~~~~ INSERT YOUR MENU  HERE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

}


void MainWindow::setupUi()
{
    setupMenu();
    setMenuBar(menu);

    progressBar = new QProgressBar();
    progressBar->setMaximumWidth(panelsWidthMin);
    progressBar->setAlignment(Qt::AlignLeft);


    progressBar->setValue(50);

    statusBar = new QStatusBar();
    statusBar->setMaximumHeight(20);
    statusBar->addWidget(progressBar);
    setStatusBar(statusBar);

    QSplitter *splitter = new QSplitter();

    layoutHorizontal = new QHBoxLayout();

    /* ~~~~~~~~~~~~~~~~~~~ LEFT TOOL PANEL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    splitterLeft = new QSplitter();
    splitterLeft->setOrientation(Qt::Vertical);

    panelLeft  = new QWidget();
    panelLeft->setMinimumWidth(panelsWidthMin);
    panelLeft->setMaximumWidth(panelsWidthMin);

    leftLayout = new QVBoxLayout(panelLeft);
    leftLayout->setAlignment(Qt::AlignTop | Qt::AlignRight);
    leftLayout->setMargin(0);
    // Here must be color woring tools' selector
    colorWheel = new ColorWheel();

    rgbWidget = new RgbWidget();

    connect(colorWheel, SIGNAL(colorChanged(QColor)), rgbWidget, SLOT(setColor(QColor)));
    connect(rgbWidget, SIGNAL(colorChanged(QColor)), colorWheel, SLOT(setColor(QColor)));

    colorWheel->setColor(Qt::yellow);

    QHBoxLayout* showCmdBox = new QHBoxLayout();

    showWheelCmd = new QPushButton("wheel");
    showWheelCmd->setToolTip(tr("Color Wheel ON / OFF"));
    showWheelCmd->setCheckable(true);
    connect(showWheelCmd, SIGNAL(clicked(bool)), colorWheel, SLOT(setShown(bool)));
    showWheelCmd->setChecked(true);
    showWheelCmd->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    showCmdBox->addWidget(showWheelCmd);

    showRgbCmd = new QPushButton("rgb");
    showRgbCmd->setToolTip(tr("RGB Slider ON / OFF"));
    showRgbCmd->setCheckable(true);
    connect(showRgbCmd, SIGNAL(clicked(bool)), rgbWidget, SLOT(setShown(bool)));
    showRgbCmd->setChecked(true);
    showRgbCmd->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    showCmdBox->addWidget(showRgbCmd);

    showCmdBox->addStretch();

    QHBoxLayout *shortToolsLayout = new QHBoxLayout();
    QGridLayout *shortToolsGrid = new QGridLayout();
    shortToolsGrid->setSpacing(1);
    shortToolsGrid->setMargin(0);
    shortToolsGrid->setRowStretch(0, 0);
    shortToolsGrid->setHorizontalSpacing(2);
    for(int i = 0; i < 5; ++i) {
        QPushButton *btt = new QPushButton();
        btt->setFixedSize(22, 22);
        QPushButton *btt1 = new QPushButton();
        btt1->setFixedSize(22, 22);
        shortToolsGrid->addWidget(btt, 0, i, Qt::AlignCenter);
        shortToolsGrid->addWidget(btt1, 1, i, Qt::AlignCenter);
    }

    // COLOR PAIR!
    colorPair = new ColorPair();
    connect(colorPair, SIGNAL(primaryColorChanged(QColor)), colorWheel, SLOT(setColor(QColor)) );
    connect(colorWheel, SIGNAL(colorChanged(QColor)), colorPair, SLOT(setPrimaryColor(QColor)) );

    shortToolsLayout->addLayout(shortToolsGrid);
    shortToolsLayout->addSpacing(20);
    shortToolsLayout->addWidget(colorPair);


    QScrollArea  *toolsScroller = new QScrollArea();
    toolsScroller->setMinimumHeight(100);

    toolGrid = new ToolsGridWidget();
    connect(toolGrid, SIGNAL(selectedTool(ToolBase*)), this, SLOT(selectTool(ToolBase*)));
    toolsScroller->setWidget(toolGrid);

    // test loading
    ToolBase* testTool  = new Pen();
    ToolBase* testTool1 = new Binary();
    ToolBase* testTool2 = new EmptyTool();
    toolGrid->addTool(0, 0, testTool);
    toolGrid->addTool(0, 1, testTool1);
    toolGrid->addTool(0, 2, testTool2);


    // something should be selected
    // kinda shit, but
    selectedTool = testTool2;

    leftLayout->addLayout(showCmdBox);
    leftLayout->addWidget(colorWheel);
    leftLayout->addWidget(rgbWidget);
    leftLayout->addLayout(shortToolsLayout);
    splitterLeft->addWidget(toolsScroller);
    leftLayout  ->addWidget(splitterLeft);

    panelLeft ->setLayout(leftLayout);


    splitter->addWidget(panelLeft);

    /* ~~~~~~~~~~~~~~~~~~~ CENTRAL WORK AREA ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    layoutVertical = new QVBoxLayout();
    toolBar        = new QWidget();
    toolBarLayout  = new QHBoxLayout();

    layoutVertical->setSpacing(3);
    layoutVertical->setMargin(2);

    toolBarLayout->setSpacing(7);
    toolBarLayout->setMargin(0);
    toolBarLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);

    undo           = new QPushButton();
    undo           ->setIcon(QIcon("./skins/basic/images/arrow_left_48.png"));
    undo           ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(undo);

    redo           = new QPushButton();
    redo           ->setIcon(QIcon("./skins/basic/images/arrow_right_48.png"));
    redo           ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(redo);

    stopSelected   = new QPushButton();
    stopSelected   ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(stopSelected);

    invertSelected = new QPushButton();
    invertSelected ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(invertSelected);

    showSelected   = new QCheckBox("Selection");
    toolBarLayout  ->addWidget(showSelected);

    zoom           = new QComboBox();
    zoom           ->setFixedSize(65, 22);
    zoom           ->addItems(QStringList({"25%", "50%", "100%", "150%", "200%", "300%", "400%","500%",
                                           "600%", "800%", "100%", "1200%", "1600%" }));
    toolBarLayout  ->addWidget(zoom);

    zoomUp         = new QPushButton();
    zoomUp         ->setIcon(QIcon("./skins/basic/images/plus_48.png"));
    zoomUp         ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(zoomUp);

    zoomDown       = new QPushButton();
    zoomDown       ->setIcon(QIcon("./skins/basic/images/minus_48.png"));
    zoomDown       ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(zoomDown);

    defaultZoom    = new QPushButton();
    defaultZoom    ->setIcon(QIcon(".//skins//basic//images//box.png"));
    defaultZoom    ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(defaultZoom);

    rotate         = new QComboBox();
    rotate         ->setFixedSize(65, 22);
    rotate         ->addItems({"0.0", "45.0", "90.0", "135.0", "180.0", "-135.0", "-90.0", "-45.0" });
    toolBarLayout  ->addWidget(rotate);

    rotateUp       = new QPushButton();
    rotateUp       ->setIcon(QIcon("./skins/basic/images/arrow_circle_left_48.png"));
    rotateUp       ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(rotateUp);

    rotateDown     = new QPushButton();
    rotateDown     ->setIcon(QIcon("./skins/basic/images/arrow_circle_right_48.png"));
    rotateDown     ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(rotateDown);

    defaultRotate  = new QPushButton();
    defaultRotate    ->setIcon(QIcon("./skins/basic/images/box.png"));
    defaultRotate    ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(defaultRotate);

    mirrored       = new QLabel("Normal");
    toolBarLayout  ->addWidget(mirrored);

    mirrorIt       = new QPushButton();
    mirrorIt       ->setIcon(QIcon("./skins/basic/images/revert.png"));
    mirrorIt       ->setFixedSize(22, 22);
    toolBarLayout  ->addWidget(mirrorIt);

    smoothLabel    = new QLabel("Stabilizer");
    toolBarLayout  ->addWidget(smoothLabel);

    strokeSmooth   = new QComboBox();
    strokeSmooth   ->setFixedSize(65, 22);
    strokeSmooth   ->addItems(QStringList({"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                                           "12", "13", "14", "15", "S-1", "S-2", "S-3", "S-4", "S-5", "S-6", "S-7"}));
    toolBarLayout  ->addWidget(strokeSmooth);

    toolBar->setLayout(toolBarLayout);
    layoutVertical->addWidget(toolBar);

    workArea = new QMdiArea();
    connect(workArea, SIGNAL(subWindowActivated(QMdiSubWindow*)), this, SLOT(subWindowActivatedSlot(QMdiSubWindow*)));


    wAreaFrame = new Frame();
    QVBoxLayout* wAreaFrameLayout = new QVBoxLayout();

    wAreaFrame      ->setLayout(wAreaFrameLayout);
    wAreaFrameLayout->addWidget(workArea);
    wAreaFrameLayout->setMargin(2);
    wAreaFrameLayout->setSpacing(0);

    layoutVertical->addWidget(wAreaFrame);

    connect(rotateUp,   SIGNAL(clicked()),      this, SLOT(rotorDown  ()) );
    connect(rotateDown, SIGNAL(clicked()),      this, SLOT(rotorUp    ()) );
    connect(rotate,     SIGNAL(activated(int)), this, SLOT(rotorByList(int)) );


    QWidget *centralW = new QWidget();
    centralW->setLayout(layoutVertical);
    splitter->addWidget(centralW);

    /* ~~~~~~~~~~~~~~~~~~~ RIGTH TOOL PANEL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


    panelRight  = new QWidget();
    panelRight->setMinimumWidth(panelsWidthMin);
    panelRight->setMaximumWidth(panelsWidthMax);

    rigthLayout = new QVBoxLayout();
    rigthLayout->setAlignment(Qt::AlignTop);
    rigthLayout->setMargin(4);

    preView = new NavigationWidget();
    connect(preView, SIGNAL(moved(QPointF)), this, SLOT(moveManual(QPointF)));

    //make icons here
    zoomLabel = new QLabel(tr("Zoom"));
    zoomSlider = new QSlider(Qt::Horizontal);
    zoomSlider->setRange(3, 1600);
    zoomSlider->setValue(100);
    zoomOutCmd = new QPushButton("-");
    zoomInCmd = new QPushButton("+");
    zoomResetCmd = new QPushButton("#");
    connect(zoomSlider,SIGNAL(valueChanged(int)), this, SLOT(scaleManual(int)));

    angleLabel = new QLabel(tr("Angle"));
    angleSlider = new QSlider(Qt::Horizontal);
    angleSlider->setRange(-180, 180);
    angleSlider->setValue(0);
    angleOutCmd = new QPushButton("-");
    angleInCmd = new QPushButton("+");
    angleResetCmd = new QPushButton("#");
    connect(angleSlider, SIGNAL(valueChanged(int)), this, SLOT(rotorManual(int)));

   // angleSlider->setStyleSheet("height: 30px;");

    QGridLayout* layerViewGrid = new QGridLayout();

    layerViewGrid->addWidget(zoomLabel,     0, 0);
    layerViewGrid->addWidget(zoomSlider,    0, 1);
    layerViewGrid->addWidget(zoomOutCmd,    0, 2);
    layerViewGrid->addWidget(zoomInCmd,     0, 3);
    layerViewGrid->addWidget(zoomResetCmd,  0, 4);

    layerViewGrid->addWidget(angleLabel,    1, 0);
    layerViewGrid->addWidget(angleSlider,   1, 1);
    layerViewGrid->addWidget(angleOutCmd,   1, 2);
    layerViewGrid->addWidget(angleInCmd,    1, 3);
    layerViewGrid->addWidget(angleResetCmd, 1, 4);

    layerTree = new LayerTreeWidget();
    connect(layerTree, SIGNAL(imageRendered(QImage)), this, SLOT(setImageForCurrentWindow(QImage)));

    layerScrollArea = new QScrollArea();
    layerScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    layerScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    layerScrollArea->setWidget(layerTree);
    layerScrollArea->setWidgetResizable(true);
    layerScrollArea->setMinimumWidth(LayerFrame::upperWidth()*12/10);
    layerScrollArea->setAlignment(Qt::AlignTop);

    QLabel* modeLabel = new QLabel(tr("Mode"));
    blendComboBox = new QComboBox();
    modeLabel->setBuddy(blendComboBox);
    blendComboBox->setToolTip(tr("Select layer blending mode"));
    blendComboBox->addItems(QStringList(Blending::blendMapper()->blendModeNames()));
    blendComboBox->setEditable(false);
    connect(blendComboBox, SIGNAL(activated(QString)), layerTree, SLOT(setBlendModeForSelected(QString)));

    QLabel* opacityLabel = new QLabel("Opacity");
    opacityBox = new QSpinBox();
    opacityLabel->setBuddy(opacityBox);
    opacityBox->setToolTip(tr("Select layer opacity"));
    opacityBox->setRange(0, 100);
    opacityBox->setValue(100);
    opacityBox->setSuffix("%");

    QGridLayout* layerGrid = new QGridLayout();
    layerGrid->addWidget(modeLabel, 0, 0);
    layerGrid->addWidget(blendComboBox, 0, 1);
    layerGrid->addWidget(opacityLabel, 1, 0);
    layerGrid->addWidget(opacityBox, 1, 1);


    //make icons here
    newLayerCmd = new QPushButton(QIcon(QDir::toNativeSeparators(".//skins//basic//images//plus_48.png")), "");
    newLayerCmd->setToolTip("New Layer");
    connect(newLayerCmd, SIGNAL(clicked()), layerTree, SLOT(addNewLayer()));

    newLayerSetCmd = new QPushButton("nls");
    newLayerSetCmd->setToolTip("New Layer Set");
    connect(newLayerSetCmd, SIGNAL(clicked()), layerTree, SLOT(addNewLayerSet()));

    deleteLayerCmd = new QPushButton(QIcon(QDir::toNativeSeparators(".//skins//basic//images//delete.png")), "");
    deleteLayerCmd->setToolTip("Delete Layer");
    connect(deleteLayerCmd, SIGNAL(clicked()), layerTree, SLOT(removeSelected()));

    QPushButton *clearLayerCmd = new QPushButton("clr");
    QPushButton *mergeLayerCmd = new QPushButton("merge");

    QHBoxLayout* layerButtons = new QHBoxLayout();
    layerButtons->addWidget(newLayerCmd);
    layerButtons->addWidget(newLayerSetCmd);
    layerButtons->addWidget(deleteLayerCmd);
    layerButtons->addWidget(clearLayerCmd);
    layerButtons->addWidget(mergeLayerCmd);

    this->setLayersEnabled(false);

    rigthLayout->addWidget(preView);
    rigthLayout->addLayout(layerViewGrid);
    rigthLayout->addLayout(layerGrid);
    rigthLayout->addLayout(layerButtons);
    rigthLayout->addWidget(layerScrollArea);

    panelRight->setLayout(rigthLayout);
    splitter->addWidget(panelRight);

    layoutHorizontal->addWidget(splitter);

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    layoutHorizontal->setMargin(2);
    layoutHorizontal->setSpacing(2);
    center = new QWidget();
    center->setLayout(layoutHorizontal);
    setCentralWidget(center);
    this->resize(sizeHint());

    /* ~~~~~~~~~~~~~~~~~~~ SHORTCUTS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


    QShortcut *fullScr = new QShortcut(QKeySequence(tr("Shift+Tab")), this);
    connect(fullScr, SIGNAL( activated() ), this, SLOT( setFullScreen() ));
    QShortcut *fullView = new QShortcut(QKeySequence(Qt::Key_Tab),  this);
    connect(fullView, SIGNAL( activated() ), this, SLOT( hidePanels() ));


}

void MainWindow::setLayersEnabled(bool b)
{
    panelRight->setEnabled(b);
}

void MainWindow::subWindowDestroyedSlot(QMdiSubWindow *mdiWindow)
{
    if (mdiWindow)
    {
        if (mdiWindow == lastSubWindow)
        {
            saveLastTreeToMemento();
        }

        auto it = treeMementoMap.find(mdiWindow);
        if (it != treeMementoMap.end())
        {
            LayerTreeWidget::Memento memento = it.value();
            memento.release();
            treeMementoMap.erase(it);
        }
    }

    if (workArea->subWindowList().size() == 0)
    {
        layerTree->resetToDefault();
        setLayersEnabled(false);
        lastSubWindow = nullptr;
    }
}


void MainWindow::subWindowActivatedSlot(QMdiSubWindow *mdiWindow)
{
    if (mdiWindow && lastSubWindow != mdiWindow)
    {
        saveLastTreeToMemento();

        auto it = treeMementoMap.find(mdiWindow);
        if (it != treeMementoMap.end())
        {
            qDebug("from Memento");
            layerTree->fromMemento(it.value());
            treeMementoMap.erase(it);
        }
        lastSubWindow = mdiWindow;
    }
}

void MainWindow::saveLastTreeToMemento()
{
    qDebug("map size: %d", treeMementoMap.size());
    if (lastSubWindow)
    {
        qDebug("to Memento");
        LayerTreeWidget::Memento memento = layerTree->toMemento();
        treeMementoMap.insert(lastSubWindow, memento);
    }
}

void MainWindow::setImageForCurrentWindow(QImage image)
{
    if (lastSubWindow)
    {
        Canvas* canv = qobject_cast<Canvas*>(lastSubWindow->widget());
        if (canv)
        {
            //canv->setImage(image);
            canv->update();
        }
    }

}


void MainWindow::rotorUp() {
    //dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->rotateUp(Canvas::ROTOR_STEP());
    //preView->rotateUp(Canvas::ROTOR_STEP());
}

void MainWindow::rotorDown() {
    //dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->rotateUp(-Canvas::ROTOR_STEP());
    //preView->rotateUp(-Canvas::ROTOR_STEP());
}

void MainWindow::rotorByList(int index) {
    rotorManual(0.0);
    rotorManual(rotate->itemText(index).toDouble());
}

void MainWindow::rotorManual(int angle) {
    //dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->rotateUp(
    //            angle - dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->getAngle()
    //            );
}

void MainWindow::scaleManual(int scale) {
    qreal tmp = 1.0f / 100.0f * scale;
    //dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->scaleUp(
    //            tmp - dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->getScale()
    //            );
}

void MainWindow::moveManual(QPointF move) {
   // dynamic_cast<Canvas*>(workArea->currentSubWindow()->widget())->moveUp(move);
}

void MainWindow::selectTool(ToolBase *value) {
    leftLayout->removeWidget(selectedTool);
    selectedTool->hide();
    selectedTool = value;
    leftLayout->addWidget(selectedTool);
    selectedTool->show();
    update();
}

void MainWindow::doAction(QList<Dot> stroke)
{
    auto layer = layerTree->getCurrentLayerFrame();
    if(layer->isLayer()) {
        // only for ratser paint tools
        selectedTool->setColor(colorPair->getPrimaryColor());
        selectedTool->action(stroke, layer->myLayerPointer()->image());
        layerTree->renderImage();
    } else {
        // for other types of tools
    }
}


void MainWindow::applySettings()
{
    // applying the shortcuts for the actions
    // TODO: make it for each by Key of the "shortcuts" map
    actions["New Canvas"]->setShortcut(QKeySequence(CONFIG({"shortcuts", "New Canvas"}).toString()));
}













