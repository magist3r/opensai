#ifndef LAYERSET_H
#define LAYERSET_H

//Qt
#include <QList>

//Own
#include "rendertree.h"
#include "abstractlayer.h"

typedef QList<AbstractLayer*> LayerList;

class LayerSet : public AbstractLayer, public LayerList
{
public:
    Layer* myLayerPointer() override;
    LayerSet* myLayerSetPointer() override;
    bool isLayerSet() const override;

    RenderTree compileRenderTree();
};

#endif // LAYERSET_H
