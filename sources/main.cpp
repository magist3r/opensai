#include <QSplashScreen>
#include <QWaitCondition>
#include <QMutex>
#include <QVariantMap>


#include "mainwindow.h"
#include "logger.h"
#include "config.h"


#define MIN_WIDTH 800
#define MIN_HEIGTH 600




int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    LogFile* logFile = new LogFile("log.txt");
    logIt.addDevice(logFile);

    if(CONFIG.loadConfig("./config"))
        logIt << Logger::Info << "Settings has been loaded successfuly" << Logger::End;
    else
        logIt << Logger::Warning << "Loaded defaults" << Logger::End;

    // Load an application style
    QFile styleFile( QDir::toNativeSeparators("./skins/" + CONFIG({"common", "theme"}).toString() + "/stylesheet.css") );
    styleFile.open( QFile::ReadOnly );

    // Apply the loaded stylesheet
    QString style( styleFile.readAll() );
    a.setStyleSheet( style );

    // splash screen
    QPixmap pixmap(":res/derpy_eyes.png");
    QSplashScreen splash(pixmap);
    splash.show();

    MainWindow *window = new MainWindow;
    //window->setupUi();
    window->setWindowIcon(QIcon(":res/icon.ico"));

    // finish splash
    window->show();
    splash.finish(static_cast<QWidget*>(window));
    
    return a.exec();
}
