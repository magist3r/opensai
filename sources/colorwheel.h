#ifndef COLORWHEEL_H
#define COLORWHEEL_H

//Qt
#include <QColor>
#include <QPainter>
#include <QPoint>
#include <QRegion>
#include <QSize>
#include <QWidget>

class QMouseEvent;
class QResizeEvent;
class QPaintEvent;

class ColorWheel : public QWidget
{
    Q_OBJECT
public:
    explicit ColorWheel(QWidget *parent = 0);
    QSize sizeHint () const override;

signals:
    QColor colorChanged(QColor color);

public slots:
    void setColor(QColor color);

protected:
    bool event(QEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
private:
    QPoint lastPos;
    bool mouseDown;
    QColor current;
    bool inWheel;
    bool inSquare;

    //Sizes
    void calculateSizes();
    int margin;
    int wheelWidth;
    QRegion wheelRegion;
    QRegion squareRegion;
    int side;   //side's length of widget
    int halfSide;
    int radius; // radius of outer wheel's bound
    int diameter;
    int colorWheelRadius;
    int innerCircleRadius;
    int outerCircleRadius;
    int halfSquareSide;
    int squareSide;

    //constants
    const qreal Pi;
    const qreal radiusFactor;
    const int angleShift;

    QColor colorFromPos(const QPoint &point);

    void preparePainter(QPainter& painter);
    void drawAll();
    void drawWheel();
    void drawSquare();
    void drawIndicator();
    void drawPicker();

};

#endif // COLORWHEEL_H
