#ifndef RGBWIDGET_H
#define RGBWIDGET_H

#include "colorslider.h"
#include <QColor>
#include <QWidget>

class RgbWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RgbWidget(QWidget *parent = 0);
    
signals:
    void colorChanged(QColor);
public slots:
    void setColor(QColor color);

protected slots:
    void emitColorChanged(int);
private:
    ColorSlider *redSlider;
    ColorSlider *greenSlider;
    ColorSlider *blueSlider;
};

#endif // RGBWIDGET_H
