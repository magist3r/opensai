/*
 *   Copyright 2013 Derptech
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * */

#ifndef TOOL_H
#define TOOL_H

// Qt
#include <QWidget>
#include <QList>
#include <QSlider>
#include <QCheckBox>
#include <QPointF>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QPainter>
#include <QPixmap>
#include <QImage>
#include <QColor>
#include <QBitmap>

// STL
#include <tuple>

// Own
#include "layer.h"
#include "blendmode.h"
#include "displaingslider.h"

using namespace Blending;


struct Dot {
	QPointF pos = { 0.0f, 0.0f };
	qreal   pressure = 0.0f;
	QPointF tilt = { 0.0f, 0.0f };
	qreal   tangental =  0.0f;
	qreal   angle = 0.0f;

	Dot();
	Dot(QPointF _pos, qreal _press, QPointF _tilt, qreal _tang, qreal _angle);
	~Dot();

	Dot operator+(const Dot& arg);
	Dot operator*(const double& arg );
	Dot& operator+=(Dot arg );
};


inline QRgb mulColor(QRgb color, qreal arg)
{
    return qRgba( qAlpha(color) * arg,
                  qRed  (color) * arg,
                  qGreen(color) * arg,
                  qBlue (color) * arg );
}

inline QRgb mulAlpha(QRgb color, qreal arg)
{
    return qRgba( qAlpha(color) * arg ,
                  qRed  (color),
                  qGreen(color),
                  qBlue (color));
}


inline QRgb addColor(QRgb color, uchar arg)
{
    return qRgba( qAlpha(color) + arg,
                  qRed  (color) + arg,
                  qGreen(color) + arg,
                  qBlue (color) + arg );
}

inline QRgb sumColors(QRgb a, QRgb b)
{
    return qRgba( qAlpha(a) + qAlpha(b),
                  qRed  (a) + qRed  (b),
                  qGreen(a) + qGreen(b),
                  qBlue (a) + qBlue (b) );
}

// Just calculating the middle of color within the region
inline QRgb middleColor(const QImage &image, const QImage &mask, const QPointF &pos, int size )
{
    unsigned int red   = 0u;
    unsigned int green = 0u;
    unsigned int blue  = 0u;

    for(int row_i = 0; row_i < size; ++row_i) {
        QRgb *line = (QRgb *)image.scanLine( row_i + pos.y() );

        for(int col_i = 0; col_i < size; ++col_i) {
            if(mask.pixel(col_i, row_i) != 0u)
                continue;

            line += ( col_i + (unsigned int)pos.x() );

            red   += qRed  (*line);
            green += qGreen(*line);
            blue  += qBlue (*line);
        }
    }
    return qRgb(red / (size * size), green / (size * size), blue / (size * size));
}


class ToolBase : public QWidget {
    Q_OBJECT
public:
    explicit ToolBase(QWidget *parent = 0)
        : QWidget(parent)
    {
        dotsPerStroke = 1;
    }
    virtual ~ToolBase() {}

    virtual bool load(QString name) = 0;
    virtual bool save(QString name) = 0;
    virtual void setupUi() = 0;

    virtual void action(QList<Dot> stroke, QImage* image) = 0;
    virtual void action(QList<Dot> stroke) = 0;

    void    setName(QString value) {name = value; }
    QString getName() { return name; }

    void setColor(QColor value);

signals:
    void layerChanged(QImage *layer);

public slots:
    void    setLayer(QImage *layer);
    QImage* getLayer() { return layer; }

protected:

    // smoothing all of the dot params
    void smoothStroke(QList<Dot> *stroke)
    {
        stroke = stroke;
    }

    mutable int dotsPerStroke;
    QImage *layer;
    QString     name;

    QColor color;

};


/**
 * @brief The paint tool
 * @abstract
 */
class PaintTool : public ToolBase {
    Q_OBJECT
public:
    explicit PaintTool(ToolBase *parent = 0)
        :ToolBase(parent)
    {}
    virtual ~PaintTool() {}

    virtual bool load(QString name) = 0;
    virtual bool save(QString name) = 0;
    virtual void setupUi() = 0;

    virtual void action(QList<Dot> stroke, QImage* image) = 0;
    virtual void action(QList<Dot> stroke) = 0;

protected:

    mutable int dotsPerStroke;
    float       edgeHardness;


    QImage shape;

    DisplaingSlider *size;
    //QSlider *size;
    QSlider *minSize;
    QSlider *density;

    QFormLayout *layout;
    QVBoxLayout *toolSettings;
};

class SelectTool : public ToolBase {

};

class TransformTool : public ToolBase {

};

#endif // TOOL_H
