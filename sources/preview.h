#ifndef PREVIEW_H
#define PREVIEW_H

#include <QtGui>

class PreviewWidget : public QWidget {
public:
     explicit PreviewWidget(QWidget *parent = 0) : QWidget(parent)
    {
        size = QSize(180, 100);
        this->setMinimumSize(size);
        setPalette(QPalette(Qt::gray));
        setAutoFillBackground(true);

    }
    ~PreviewWidget() {}


protected:

    virtual void paintEvent(QPaintEvent* )
    {
        drawPreview();
        //update();
    }


    virtual void resizeEvent(QResizeEvent *)
    {
        update();
    }

private:
    // should to crate slot/signal for changing following
    QImage *buffer;
    QImage *preView;
    QSize size;
    QSize canvasSize;
    QPoint canvasPos;
    qreal canvasAgnle;


    void drawPreview() {
        buffer = new QImage(size, QImage::Format_ARGB32);
        buffer->fill(Qt::gray);
        QImage tempImage(size, QImage::Format_ARGB32);
        QPainter painter(buffer);

        QPoint point(size.width() - canvasSize.width(), size.height() - canvasSize.height());
        painter.drawImage( point, *buffer);

        QPainter tempPainter(&tempImage);
        tempPainter.setRenderHint(QPainter::Antialiasing);
        QPen pen(Qt::red);
        pen.setStyle(Qt::DashLine);
        pen.setWidth(1);
        tempPainter.setPen(pen);
        tempPainter.drawRect(canvasPos.x(), canvasPos.y(), canvasSize.width(), canvasSize.height());
        tempPainter.rotate(canvasAgnle);

        painter.drawImage(0, 0, tempImage);
    }

};

#endif // PREVIEW_H
