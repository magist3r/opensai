OpenSAI
=======
OpenSAI is a raster/vector graphic editor. Mostly, clone of Easy Paint Tool SAI which was made by SYSTEMAX 
and was advanced no more. 
So, because there're less than nothing of really good _drawing_ software in open source, we have started to
develop this program.

======
Our goals: 
  1. Fully identic to SAI functional at first.
  2. More customizability for tools and user interface.
  3. Plugins system. (based on python)
  4. Light and fast program.

======
Rules:

Always write a comment when committing something to the repository. Your comment should be brief 
and to the point, describing what was changed and possibly why. If you made several changes, 
write one line or sentence about each part. If you find yourself writing a very long list of 
changes, consider splitting your commit into smaller parts, as described earlier. Prefixing 
your comments with identifiers like Fix or Add is a good way of indicating what type of change 
you did. It also makes it easier to filter the content later, either visually, by a human reader, 
or automatically, by a program.

If you fixed a specific bug or implemented a specific change request, it also recommend to 
reference the bug or issue number in the commit message.

Just follow next scheme: [Keyword]: [Description] 

Here are some examples of good commit messages:
    
    Add: CLoader's method "loadFromFile" for loading file.
    Fix: Extra image removed.
    Fix: Fixed bug #9000.
    Add: Implemented change request #39381.
